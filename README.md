## Frappe Utils

Utility tools and more around frappe framework

# Getting Started

To get started, you will need install frappe_utils just like any other frappe app.

```sh
# Fetch frappe utils
bench get-app https://gitlab.com/castlecraft/frappe_utils --branch main

# Install to your site
bench --site YOUR_SITE install-app frappe_utils

# Install dependencies
bench setup requirements frappe_utils

# Update schema and patches
bench --site YOUR_SITE migrate
```

## Table of Contents

- [Paginated Child Tables](https://castlecraft.gitlab.io/frappe-manual/framework-extensions/doctypes/paginated-child-table)
- [Frappe Permission Parser](https://castlecraft.gitlab.io/frappe-manual/framework-extensions/code/frappe-perm-parser)
- [Single App Migration](https://castlecraft.gitlab.io/frappe-manual/framework-extensions/code/single-app-migration)
- [Sync Masters](https://castlecraft.gitlab.io/frappe-manual/framework-extensions/doctypes/master-meta-and-migration/#sync-masters)
- [Custom Auth Provider (SSO)](https://castlecraft.gitlab.io/frappe-manual/framework-extensions/authorization/custom-auth)
- [Composite Key](https://castlecraft.gitlab.io/frappe-manual/framework-extensions/doctypes/composite-key)

## License

This repository has been released under the [MIT License](LICENSE).
