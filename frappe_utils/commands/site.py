import json
import os
import sys

import click
import frappe
from frappe import get_module
from frappe.commands import pass_context
from frappe.core.doctype.language.language import sync_languages
from frappe.core.doctype.scheduled_job_type.scheduled_job_type import sync_jobs
from frappe.deferred_insert import save_to_db as flush_deferred_inserts
from frappe.exceptions import SiteNotSpecifiedError

from frappe_utils.common.util import sync_json_to_schema


@click.command("migrate-app")
@click.argument("apps", nargs=-1)
@click.option(
    "--skip-failing",
    is_flag=True,
    help="Skip patches that fail to run",
)
@click.option(
    "--skip-search-index",
    is_flag=True,
    help="Skip search indexing for web documents",
)
@click.option(
    "--force",
    is_flag=True,
    help="Force migration even if is_set_to_migrate unset",
)
@pass_context
def migrate_app(
    context,
    apps,
    skip_failing=False,
    skip_search_index=False,
    force=False,
):
    "Run migrate for single app"
    from frappe_utils.migrate import PartialSiteMigration

    for site in context.sites:
        if site:
            frappe.init(site=site)
            frappe.connect()

            # Run once per site
            sync_jobs()
            sync_languages()
            flush_deferred_inserts()

        click.secho(f"Migrating {site}", fg="green")
        for app in apps:
            app_module = None
            try:
                app_module = get_module(app)
            except ModuleNotFoundError:
                click.secho(f"App {app} does not exist", fg="red")
                sys.exit(1)

            if (
                not getattr(app_module, "is_set_to_migrate", None)
                and not force  # noqa: E501
            ):  # noqa: E501
                click.secho(f"App {app} not set to migrate", fg="yellow")
                sys.exit()

            click.secho(f"Migrating {app}", fg="green")
            try:
                PartialSiteMigration(
                    skip_failing=skip_failing,
                    skip_search_index=True,
                ).run_app(site=site, app=app)

            finally:
                print()  # noqa: T201, T001

        if not skip_search_index:
            print(  # noqa: T201, T001
                f"Queued rebuilding of search index for {frappe.local.site}"
            )
        #     frappe.enqueue(build_index_for_all_routes, queue="long")
        frappe.destroy()

        if not context.sites:
            raise SiteNotSpecifiedError


@click.command("sync-masters")
@click.argument("master_doctype", nargs=-1)
@click.option(
    "--force",
    is_flag=True,
    help="Force sync even if version are same",
)
@pass_context
def sync_masters(context, master_doctype=[], force=False):

    for site in context.sites:
        if site:
            frappe.connect(site=site)
            click.secho("Syncing Masters...", fg="green")

            execute_sync_masters(master_doctype=master_doctype, force=force)


def get_all_linked_doctypes(master_files):
    def file_name_to_doctype(filename):
        # Split the file name into words using underscores
        words = filename.split("_")

        # Capitalize the first letter of each word and join them with a space
        return " ".join(word.capitalize() for word in words)

    all_linked_doctypes = {}
    doc_names = [file_name_to_doctype(doc_name) for doc_name in master_files]

    for doctype in doc_names:
        doc_field = frappe.get_all(
            "DocField",
            filters=[
                ["parent", "=", doctype],
                ["fieldtype", "=", "Link"],
            ],
            fields=["options"],
        )

        # Extract linked doctypes from DocField
        linked_doctypes = [
            item["options"].lower().replace(" ", "_") for item in doc_field
        ]

        # Convert to lowercase and replace spaces with underscores
        doctype_key = doctype.lower().replace(" ", "_")

        all_linked_doctypes[doctype_key] = linked_doctypes

    return all_linked_doctypes


def execute_sync_masters(master_doctype=[], force=False):
    for app in frappe.get_installed_apps():
        masters_path = frappe.get_app_path(app, "masters")

        if not os.path.exists(masters_path):
            continue

        if master_doctype:
            masters_files = []
            for doc in master_doctype:
                mas_doc = frappe.scrub(doc)
                if os.path.exists(os.path.join(masters_path, mas_doc)):
                    masters_files.append(mas_doc)
        else:
            masters_files = os.listdir(masters_path)

        get_linked_doctypes = get_all_linked_doctypes(masters_files)
        sequence = []

        for fname in masters_files:
            if fname not in sequence:
                sequence.append(fname)

            linked_doctypes = get_linked_doctypes.get(fname)

            for doctype in linked_doctypes:
                if doctype in masters_files:
                    if doctype in sequence:
                        sequence.remove(doctype)

                    if doctype in sequence:
                        index_fname = sequence.index(fname)
                        sequence.insert(index_fname, doctype)
                    else:
                        sequence.append(doctype)

        for queue in sequence:
            doctype_folder_name = frappe.get_app_path(
                app, "masters", queue
            )  # noqa: 501

            try:
                process_doctype_folder(doctype_folder_name, force)
                frappe.db.commit()
            except Exception:
                frappe.log_error(
                    title="Sync Master Error",
                    message=frappe.get_traceback(True),  # noqa: 501
                )


def process_doctype_folder(doctype_folder_name, force=False):
    data_json_path = os.path.join(doctype_folder_name, "data.json")
    meta_json_path = os.path.join(doctype_folder_name, "meta.json")

    if not os.path.exists(data_json_path):
        return

    data_json = load_json_file(data_json_path)
    meta_json = load_json_file(meta_json_path)

    exclude_update = meta_json.get("exclude_update", [])

    exclude_import = meta_json.get("exclude_import", [])
    if not data_json:
        return

    doctype_name = data_json[0].get("doctype")
    if force or check_version_bumped(doctype_name, meta_json):
        try:
            frappe.get_cached_doc("DocType", doctype_name)
        except frappe.DoesNotExistError:
            # Handle the case where the doctype is not found
            frappe.log_error(
                frappe.get_traceback(),
                f"DoesNotExistError: '{doctype_name}' DocType",  # noqa: 501
            )
            return

        for entry in data_json:
            # Check if the document exists in the database
            # If not then consider dict from json dump
            if frappe.db.exists(doctype_name, entry.get("name")):
                doc = frappe.get_cached_doc(
                    doctype_name, entry.get("name")
                ).as_dict()  # noqa: 501
            else:
                doc = entry
            post_process([doc])

            # take excluded update fields values from db
            for ex in exclude_update:
                entry[ex] = doc.get(ex)

            # exclude import fields only if the document is fromdoc
            for im in exclude_import:
                if frappe.db.exists(doctype_name, entry.get("name")):
                    entry[im] = doc.get(im)
                else:
                    entry[im] = None

        sync_json_to_schema(data_json)
        sync_to_frappe_util_settings(doctype_name, meta_json)


def sync_to_frappe_util_settings(doctype_name, meta_json):
    fr_ut_setting_doc = frappe.get_doc("Frappe Util Settings")

    master_meta_table = fr_ut_setting_doc.get("master_meta_table")

    existing_meta = next(
        (x for x in master_meta_table if x.meta_doctype == doctype_name), None
    )
    if existing_meta:
        # Update values if record exists
        for record in master_meta_table:
            if record.meta_doctype == existing_meta.meta_doctype:
                record.last_modified = meta_json.get("latest_modified")
                record.version = meta_json.get("version")
                fr_ut_setting_doc.set("master_meta_table", master_meta_table)
        fr_ut_setting_doc.save()

    else:

        # Append the new record to the table
        fr_ut_setting_doc.append(
            "master_meta_table",
            {
                "meta_doctype": doctype_name,
                "last_modified": meta_json.get("latest_modified"),
                "version": meta_json.get("version"),
            },
        )

        # Save the parent document
        fr_ut_setting_doc.save()


def check_version_bumped(doctype_name, meta_json):
    # Check if record already exists in master_meta_table
    frappe_util_setting_doc = frappe.get_doc("Frappe Util Settings")

    master_meta_table = frappe_util_setting_doc.get("master_meta_table")
    existing_meta = next(
        (x for x in master_meta_table if x.meta_doctype == doctype_name), None
    )
    if existing_meta:
        return not (existing_meta.version == meta_json.get("version"))
    else:
        return True


# copied from frappe.core.doctype.data_import.data_import
def post_process(out):
    # Note on Tree DocTypes:
    # The tree structure is maintained in the db via the fields
    # "lft" and "rgt".
    # They are automatically set and kept up-to-date.
    # Importing them would destroy any existing tree structure.
    # For this reason they are not exported as well.
    del_keys = (
        "modified_by",
        "creation",
        "owner",
        "idx",
        "lft",
        "rgt",
    )  # noqa: 501
    for doc in out:
        for key in del_keys:
            if key in doc:
                del doc[key]
        for k, v in doc.items():
            if isinstance(v, list):
                for child in v:
                    for key in del_keys + (
                        "docstatus",
                        "doctype",
                        "modified",
                        "name",
                    ):
                        if key in child:
                            del child[key]


def load_json_file(path):
    if os.path.exists(path):
        with open(path) as file:
            return json.load(file)
    return {}
