from frappe_utils.commands.site import migrate_app, sync_masters

commands = [migrate_app, sync_masters]
