/* global frappe */

frappe.ui.form.on('User Permission', {
  refresh: frm => {
    handleActivePermissionRule(frm);
  },
});

function handleActivePermissionRule(frm) {
  if (frm.doc.custom_has_permission_rule) {
    frm.disable_form();
    frm.set_intro('');
    frm.set_intro(
      'Following permissions are read-only as they have a active Permission Rule',
    );
  }
}
