frappe.listview_settings['RQ Job'] = {
  refresh(listview) {
    listview.page.add_inner_button('Flush Queue', function () {
      frappe.call({
        method: 'frappe_utils.common.background_jobs.get_all_queue',
        callback: r => {
          if (r.message) {
            const d = new frappe.ui.Dialog({
              title: 'Select Queue',
              fields: [
                {
                  label: 'Queue',
                  fieldname: 'queue',
                  fieldtype: 'Select',
                  options: r.message,
                  reqd: 1,
                },
              ],
              size: 'small',
              primary_action_label: 'Submit',
              primary_action(values) {
                d.hide();
                frappe.call({
                  method:
                    'frappe_utils.common.background_jobs.remove_enqueued_jobs',
                  args: { queues: values.queue },
                  freeze: true,
                  callback: res => {
                    listview.refresh();
                    frappe.msgprint(
                      `Queue '${values.queue}' successfully flushed`,
                    );
                  },
                });
              },
            });
            d.show();
          }
        },
      });
    });
  },
};
