import datetime

import frappe


def clear_expiry_token():
    def expire_token(before_timestamp):
        oauth_bearer_token = frappe.qb.DocType("OAuth Bearer Token")
        query = (
            frappe.qb.from_(oauth_bearer_token)
            .delete()
            .where(oauth_bearer_token.expiration_time < before_timestamp)
        )
        query.run()
        frappe.db.commit()

    frappe_util_setting = frappe.get_doc("Frappe Util Settings")
    expiration_duration = frappe_util_setting.get(
        "oauth_bearer_token_expiration", "2 week"
    )
    now = datetime.datetime.now()

    if expiration_duration == "1 day":
        expiration = now - datetime.timedelta(days=1)

    elif expiration_duration == "1 week":
        expiration = now - datetime.timedelta(days=7)

    elif expiration_duration == "2 week":
        expiration = now - datetime.timedelta(days=14)

    elif expiration_duration == "1 month":
        expiration = now - datetime.timedelta(days=30)

    expire_token(expiration)
