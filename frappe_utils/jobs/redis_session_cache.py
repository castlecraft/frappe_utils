import frappe
from frappe.sessions import get_expired_threshold

DEFAULT_BATCH_SIZE = 10000


def clear_expired_sessions_as_batch():
    """This function is meant to be called from scheduler"""
    frappe.log("Session clear started.")
    BATCH = frappe.conf.get("SESSIONS_CLEANUP_BATCH_SIZE", DEFAULT_BATCH_SIZE)

    while BATCH:
        sessions = get_expired_sessions(BATCH)

        BATCH = 0 if len(sessions) < BATCH else BATCH
        if not sessions:
            break

        delete_redis_session(sessions)
        frappe.log(f"cleaned up {len(sessions)} sessions from redis.")

        purge_db_sessions(sessions)
        frappe.log(f"cleaned up {len(sessions)} sessions from database.")
        frappe.log("Batch completed.")

    frappe.log("Session clear completed.")


def get_expired_sessions(limit):
    """Returns list of expired sessions"""
    sessions = frappe.qb.DocType("Sessions")
    expired = []
    expired.extend(
        (
            frappe.qb.from_(sessions)
            .select(sessions.sid)
            .where(
                (sessions.lastupdate < get_expired_threshold("desktop"))
                & (sessions.device == "desktop")
            )
            .limit(limit)
        ).run(pluck=True)
    )

    return expired


def delete_redis_session(sessions):
    if frappe.flags.read_only:
        # This isn't manually initiated logout, most likely user's
        # cookies were expired in such case
        # we should just ignore it till database is back up again.
        return

    for sid in sessions:
        frappe.cache().hdel("session", sid)
        frappe.cache().hdel("last_db_session_update", sid)


def purge_db_sessions(sessions):
    session = frappe.qb.DocType("Sessions")
    query = frappe.qb.from_(session).delete().where(session.sid.isin(sessions))
    query.run()
    frappe.db.commit()
