import unittest

import frappe

from frappe_utils.test_helper.user_helper import create_user
from frappe_utils.test_helper.util_testing_helper import UtilTestingHelper


class FrappeTestingBase(unittest.TestCase):

    frappe.set_user("Administrator")
    exists = frappe.db.exists("User", "anonymous@gmail.com")

    if not exists:
        # Create a Dummy User with System Manager rights
        # if not present already and forget it
        create_user("Anonymous", "anonymous@gmail.com")

    def tearDown(self):
        UtilTestingHelper().purge_tear_down()
