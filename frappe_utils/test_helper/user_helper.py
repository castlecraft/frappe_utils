import frappe

from frappe_utils.test_helper.util_testing_helper import UtilTestingHelper


def create_user(name, email, roles=[{"role": "System Manager"}]):
    user_doc = frappe.get_doc(
        {
            "doctype": "User",
            "first_name": name,
            "email": email,
            "roles": roles,
        }
    )
    user_doc.insert()
    if user_doc.first_name != "Anonymous":
        # Do not delete the Dummy User
        UtilTestingHelper().append_tear_down_doc("User", user_doc.email)
