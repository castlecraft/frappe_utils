import frappe

# Sequence is IMPORTANT
state = {
    # [KEY = Doctype Name]: Value = {
    #   doc_names: [] array of created docs,
    #   bootstrap: True/False,
    # }
}

tear_down_sequece = []


class UtilTestingHelper:
    def initialize_tear_down_sequence(self, doc_names=[]):
        for key in doc_names:
            tear_down_sequece.add(key)
            if not state.get(key):
                state[key] = {"doc_names": [], "bootstrap": False}

    def append_tear_down_doc(self, doctype, doc_name):
        if state.get(doctype) and len(state[doctype]) != 0:
            state[doctype]["doc_names"].append(doc_name)
        else:
            state[doctype] = {"doc_names": [doc_name], "bootstrap": True}
            tear_down_sequece.append(
                doctype
            ) if doctype not in tear_down_sequece else True

    # DB Cleanup process
    def purge_tear_down(self):
        frappe.set_user("Administrator")

        # Remove any defined sequence
        for doctype in reversed(list(tear_down_sequece)):
            if doctype != "DocType":
                for doc_name in state[doctype]["doc_names"]:
                    self.delete_doc(doctype, doc_name)

        # Remove all other doctypes
        for doctype, doc_state in state.items().__reversed__():
            if doctype != "DocType":
                for doc_name in doc_state["doc_names"]:
                    self.delete_doc(doctype, doc_name)

        # Remove any create doctype
        if state.get("DocType"):
            for doc_name in state["DocType"]["doc_names"]:
                self.delete_doc(doctype, doc_name)

    # TODO: Append support for throw error
    def delete_doc(self, doctype, doc_name, throw_error=False):
        try:
            frappe.delete_doc(doctype, doc_name)
        except Exception as e:
            print(  # noqa: T001
                "[purge_tear_down] Failed to purge doctype "
                f"{doctype} name: {doc_name}"  # noqa: 501
            )
            print(e)  # noqa: T001
            pass

    def get_sample_doc_names(self, doctype):
        return state[doctype]["doc_names"] if state.get(doctype) else list()
