from typing import Callable

import frappe

from frappe_utils.test_helper.util_testing_helper import (  # noqa: E501 #isort: skip
    UtilTestingHelper,
    state,
)


def bootstrap_doctype(doctype) -> Callable:
    def decorator(func):
        def wrapper(method=None):
            if state.get(doctype) and state[doctype]["bootstrap"] is True:
                print(  # noqa: T001
                    f"[bootstrap_doctype] {doctype} already bootstrapped."
                )  # noqa: T001, 501
                return

            return func(method)

        return wrapper

    return decorator


def spin_examples(doctype) -> Callable:
    def decorator(func):
        def wrapper(method=None):
            docs = func()
            for doc in docs:
                new_doc = frappe.get_doc(doc)
                new_doc.insert()
                UtilTestingHelper().append_tear_down_doc(doctype, new_doc.name)

            return True

        return wrapper

    return decorator
