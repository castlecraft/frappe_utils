import frappe

from frappe_utils.request_validator.common_function import RequestAttributes
from frappe_utils.request_validator.request_validator import request_validator

from frappe_utils.request_validator.EXAMPLE.api_model import (  # isort: skip
    PersonSchema,
    PersonSchemaCustom,
)


def parser(e):
    return "custom parsing, something went wrong here"


# Custom Error parsing with overwritten parser
@frappe.whitelist(methods=["POST"])
@request_validator(
    PersonSchemaCustom,
    {
        "validation_error_parser": parser,
        "request_attributes": [
            RequestAttributes.body,
            RequestAttributes.user,
            RequestAttributes.request,
            RequestAttributes.query,
        ],
    },
)
def custom_ping(*args, **kwargs):
    body = kwargs.get(RequestAttributes.body.value)
    user = kwargs.get(RequestAttributes.user.value)
    query = kwargs.get(RequestAttributes.query.value)
    request = kwargs.get(RequestAttributes.request.value)  # noqa: F841

    return body, user, query


@frappe.whitelist(methods=["POST"])
@request_validator(PersonSchema)
def ping(*args, **kwargs):
    body = kwargs.get(RequestAttributes.body.value)

    return body
