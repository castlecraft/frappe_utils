from marshmallow import Schema, ValidationError, fields, validates_schema


class AddressSchema(Schema):
    street = fields.Str(required=True, validate=lambda s: len(s) > 0)
    city = fields.Str(required=True, validate=lambda s: len(s) > 0)
    zip_code = fields.Str(required=True, validate=lambda s: len(s) > 0)


class PersonSchema(Schema):
    name = fields.Str(
        required=True,
    )
    age = fields.Int(required=True)
    email = fields.Email(required=True)
    addresses = fields.List(fields.Nested(AddressSchema), required=True)


# Schema with custom validation and allowed unknown fields
class PersonSchemaCustom(Schema):
    name = fields.Str(required=True, validate=lambda s: len(s) > 0)
    age = fields.Int(required=True, validate=lambda n: 18 <= n <= 150)
    email = fields.Email(required=True)
    addresses = fields.List(fields.Nested(AddressSchema), required=True)

    # Custom validation for any field
    @validates_schema
    def validate_addresses(self, data, **kwargs):
        addresses = data.get("addresses")
        if not addresses or len(addresses) == 0:
            raise ValidationError("Addresses must not be empty")

    # To allow fields that are not defined in schema
    class Meta:
        unknown = True
