from enum import Enum

import frappe
from frappe.api.v1 import get_request_form_data


class RequestAttributes(Enum):
    def __str__(self):
        return str(self.value)

    body = "body"
    request = "request"
    query = "query"
    user = "user"


def get_frappe_request():
    return {
        RequestAttributes.body: get_request_form_data(),
        RequestAttributes.query: frappe.request.args
        if frappe.request.args
        else {},  # noqa: E501
        RequestAttributes.user: frappe.session.user
        if frappe.session.user
        else None,  # noqa: E501
    }
