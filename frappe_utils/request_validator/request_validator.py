from typing import Callable, TypedDict

from marshmallow import ValidationError
from typing_extensions import NotRequired

from frappe_utils.auth import throw_invalid_request
from frappe_utils.common.util import deep_merge

from frappe_utils.request_validator.common_function import (  # isort: skip
    RequestAttributes,
    get_frappe_request,
)


class Config(TypedDict):
    # Overwrite function for adding custom error parsing
    validation_error_parser: NotRequired[Callable[[ValidationError], str]]

    # Attributes to receive from request mapping as part of `kwargs`
    request_attributes: NotRequired[list[RequestAttributes]]


def __get_validator_error_message(e: ValidationError) -> str:
    error_messages = []

    for field, errors in e.messages.items():
        error_messages.append(
            f"Field: {field}, Errors: {', '.join(errors) if type(errors) is str else errors}"  # noqa: E501
        )

    return "\n".join(error_messages)


default_overwrite_options: Config = {
    "validation_error_parser": __get_validator_error_message,
    "request_attributes": [RequestAttributes.body],
}


def request_validator(schema, options: Config = default_overwrite_options):
    overwrite_options = deep_merge(default_overwrite_options, options)

    def decorator(func):
        def wrapper(*args, **kwargs):
            request = get_frappe_request()

            is_request_valid = __validate_request_schema(
                schema, request, overwrite_options
            )
            if is_request_valid:
                for key in overwrite_options.get("request_attributes") or []:
                    if key == RequestAttributes.request:
                        kwargs[key.value] = request
                    else:
                        kwargs[key.value] = request.get(key)

                return func(*args, **kwargs)

        return wrapper

    return decorator


def __validate_request_schema(schema, request, overwrite_options):
    try:
        schema().load(request.get(RequestAttributes.body))
        return True
    except ValidationError as e:
        throw_invalid_request(
            "Failed to validate request",
            400,
            overwrite_options.get("validation_error_parser")(e),
        )
        return False
