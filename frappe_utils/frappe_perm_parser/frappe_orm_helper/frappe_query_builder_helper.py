import frappe
from frappe.desk.form.linked_with import get_linked_doctypes

# Returns set of all tables referenced in query builder


def extract_tables(query):
    tables = set()

    if isinstance(query, list):
        for item in query:
            tables.update(extract_tables(item))
    elif isinstance(query, dict):
        for key, value in query.items():
            if key == "table":
                tables.add(value)
            elif isinstance(value, (dict, list)):
                tables.update(extract_tables(value))

    return tables


# Appends a custom condition to frappe query builder for each table referenced
# TODO: Needs more update around conditions WIP


def add_custom_condition(query, table_names=None, param={}):
    if table_names is None:
        table_names = set()

    if isinstance(query, list):
        for item in query:
            add_custom_condition(item, table_names)
    elif isinstance(query, dict):
        for key, value in query.items():
            if key == "table":
                table_name = value
                if table_name not in table_names:
                    query["and"] = query.get("and", [])
                    query["and"].append(
                        {
                            f"{table_name}.{param.get('key')}": param.get(
                                "value"
                            )  # noqa: E501
                        }
                    )
                    table_names.add(table_name)
            elif isinstance(value, (dict, list)):
                add_custom_condition(value, table_names)


def append_query_condition(qb, doctype, condition={}):
    doc = frappe.qb.DocType(doctype)

    return qb.where(
        getattr(
            doc,
            f"""{
                condition.get('fieldname')
                if isinstance(condition.get('fieldname'), str)
                else condition.get('fieldname')[0]
            }""",
        ).isin(
            condition.get("value")
        )  # flake8: skip
    )


def get_linked_doc_references(doctype, include_doctype=[]):
    # Remove doctype reference which dont have a table reference
    linked_doctypes = get_linked_doctypes(doctype, True)

    # Append self as a reference.
    linked_doctypes[doctype] = {"fieldname": ["name"]}

    return (
        {k: v for k, v in linked_doctypes.items() if k in include_doctype}
        if len(include_doctype) > 0
        else get_linked_doctypes(doctype)
    )
