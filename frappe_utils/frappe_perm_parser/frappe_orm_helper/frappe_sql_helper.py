from enum import Enum

import sqlparse

from frappe.core.doctype.user_permission.user_permission import (  # noqa: E501 isort: skip
    get_user_permissions,
)


class PermHash(Enum):
    specific = 0
    all = 1
    self = 2


# Returns a hashmap of all linked field for a doc
# {  [Doctype name]: 'linked_key' }
#
# Example (Invoice_Doc)
#  {
#   "Warehouse": 'warehouse',
#   "Customer": 'customer',
# }
def get_linked_fields_and_doctypes_hash(doctype_meta):
    linked_fields = {}

    for field in doctype_meta.fields:
        if field.fieldtype == "Link":
            linked_fields[field.options] = field.fieldname

    return linked_fields


# Returns set of doctype names from provided list of table names


def get_linked_doctypes(query=""):
    table_names = set()
    parsed = sqlparse.parse(query)

    # TODO: We need a way to filter out field names dynamically.
    for stmt in parsed:
        for token in stmt.tokens:
            if isinstance(token, sqlparse.sql.IdentifierList):
                for identifier in token.get_identifiers():
                    table_names.add(identifier.get_real_name())

            elif isinstance(token, sqlparse.sql.Identifier):
                table_names.add(token.get_real_name())

        # We may need a throw depending on cases where a
        # invalid input was provided or we encounter an edge case

        # else:
        #       handle something..

    # Parse non frappe table names or field names
    return [item[3:] for item in table_names if item.startswith("tab")]


# Returns a hash of user permissions as per restricted doc`{ "table_name" : ["doc_1","doc_2"]}` # noqa: E501


def get_user_permissions_as_hash(user, doctypes=[]):
    result = {PermHash.all: {}, PermHash.specific: {}, PermHash.self: {}}

    user_permissions = get_user_permissions(user)

    for key, value in user_permissions.items():
        for perm_items in value:
            applicable_for = perm_items.get("applicable_for")

            if applicable_for is None:
                if key in result[PermHash.all]:
                    result[PermHash.all][key].append(
                        perm_items.get("doc")
                    )  # noqa: E501
                else:
                    result[PermHash.all][key] = [perm_items.get("doc")]

            elif key == applicable_for:
                if key in result[PermHash.self]:
                    result[PermHash.self][key].append(
                        perm_items.get("doc")
                    )  # noqa: E501
                else:
                    result[PermHash.self][key] = [perm_items.get("doc")]

            elif applicable_for in doctypes:
                if key in result[PermHash.specific]:
                    result[PermHash.specific][key].append(perm_items)  # noqa: E501
                else:
                    result[PermHash.specific][key] = [perm_items]

    # Clean hash
    for key, value in result.items():
        result[key] = {
            key: value for key, value in result[key].items() if value
        }  # noqa: E501

    return result
