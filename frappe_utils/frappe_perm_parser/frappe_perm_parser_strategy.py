from abc import ABC, abstractmethod


# Define a base strategy interface or abstract class
class FrappePermParserStrategy(ABC):
    @abstractmethod
    def get_doctype_list(self, qb=None):
        pass

    @abstractmethod
    def append_field_level_control(self, qb=None, doctypes=[], user=None):
        pass

    @abstractmethod
    def append_global_level_control(self, qb=None, doctypes=[], user=None):
        pass


class FrappePermParserContext:
    """
    The Context defines the interface of interest to clients.
    """

    def __init__(self, strategy: FrappePermParserStrategy) -> None:
        """
        Usually, the Context accepts a strategy through the constructor, but
        also provides a setter to change it at runtime.
        """

        self._strategy = strategy

    @property
    def strategy(self) -> FrappePermParserStrategy:
        """
        The Context maintains a reference to one of the Strategy objects. The
        Context does not know the concrete class of a strategy. It should work
        with all strategies via the Strategy interface.
        """

        return self._strategy

    @strategy.setter
    def strategy(self, strategy: FrappePermParserStrategy) -> None:
        """
        Usually, the Context allows replacing a Strategy object at runtime.
        """

        self._strategy = strategy

    def get_doctype_list(self, qb=None):
        return self._strategy.get_doctype_list(qb)

    def append_field_level_control(self, qb=None, doctypes=[], user=None):
        return self._strategy.append_field_level_control(qb, doctypes, user)

    def append_global_level_control(self, qb=None, doctypes=[], user=None):
        return self._strategy.append_global_level_control(qb, doctypes, user)
