import frappe
from frappe import _
from pypika.dialects import MySQLQueryBuilder

from frappe_utils.frappe_perm_parser.strategies.pure_query_strategy import (  # noqa: E501 isort: skip
    PureQueryStrategy,
)
from frappe_utils.frappe_perm_parser.strategies.query_builder_strategy import (  # noqa: E501 isort: skip
    QueryBuilderStrategy,
)


def has_valid_perms(
    qb=None,
    exclude=[],
    user=None,
    ptype="read",
    raise_exception=False,
    append_query_validations=True,
):
    """
    Validates frappe permissions for provided query builder query
    Returns query if the user has permission `ptype` for referenced
    tables in query.

    :param qb: Query builder instance or pure SQL query.
    :param exclude: Doctype names that should bypass validation.
    :param user: [optional] Check for given user. Default: current user.
    :param ptype: Permission type (read, write, create, submit, cancel, amend). Default: read.
    :param raise_exception: Raises `frappe.PermissionError` if user isn't permitted and `throw` is truthy
    :param append_query_validations: Appends field level and global permission validation to parent query
    """  # noqa: E501

    doctypes = []
    has_permissions = False
    doctypes = get_strategy_context(qb).get_doctype_list(qb)

    if len(doctypes) == 0:
        frappe.throw(_("Invalid query, no table reference detected."))

    # Remove excluded ones with Outer join.
    doctypes = [x for x in doctypes if x not in exclude]

    # Iterate over provided doctypes and check if session user has permissions
    for doc in doctypes:

        # following function will make sure to validate
        # child table permissions as well.

        has_permissions = frappe.has_permission(
            doctype=doc, user=user, ptype=ptype, throw=raise_exception
        )

        if not has_permissions:
            break

    if append_query_validations and has_permissions:
        return append_query_filters(qb, doctypes, user)

    return has_permissions


def append_query_filters(qb=None, doctypes=[], user=None):
    """
    Append required filters for query builder as per provided custom
    permissions on doctype

    Applies:
        field_level_control:
            Doc level permissions such as user A has access to customer X, Y, Z
            User A has access to country India for all linked docs
        global_level_control:
            User has permissions only to view docs he created
    """

    qb = get_strategy_context(qb).append_field_level_control(
        qb, doctypes, user
    )  # noqa: E501
    qb = get_strategy_context(qb).append_global_level_control(
        qb, doctypes, user
    )  # noqa: E501

    return qb


def get_strategy_context(qb=None):
    if isinstance(qb, MySQLQueryBuilder):
        return QueryBuilderStrategy()
    elif isinstance(qb, str):
        return PureQueryStrategy()
    else:
        frappe.throw(
            _(
                "Invalid query, please provide either a builder query or pure SQL as string.",  # noqa: E501
            )
        )
