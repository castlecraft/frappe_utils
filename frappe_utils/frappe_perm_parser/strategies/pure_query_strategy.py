import frappe
from frappe import _

from frappe_utils.frappe_perm_parser.frappe_orm_helper.frappe_sql_helper import (  # noqa: E501 isort: skip
    get_linked_doctypes,
)
from frappe_utils.frappe_perm_parser.frappe_perm_parser_strategy import (  # noqa: E501 isort: skip
    FrappePermParserStrategy,
)


class PureQueryStrategy(FrappePermParserStrategy):

    # returns list of doctypes from provided pure query
    def get_doctype_list(self, qb=None):

        return get_linked_doctypes(qb.get_sql())

    def append_field_level_control(qb=None, doctypes=[], user=None):
        frappe.throw(
            _(
                "[PureQueryStrategy][append_field_level_control] Unsupported field control for pure query, please use query builder",  # noqa: E501
            )
        )

    def append_global_level_control(qb=None, doctypes=[], user=None):
        frappe.throw(
            _(
                "[PureQueryStrategy][append_field_level_control] Unsupported field control for pure query, please use query builder",  # noqa: E501
            )
        )
