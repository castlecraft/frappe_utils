from frappe_utils.frappe_perm_parser.frappe_orm_helper.frappe_sql_helper import (  # noqa: E501 isort: skip
    get_user_permissions_as_hash,
    get_linked_doctypes,
    PermHash,
)
from frappe_utils.frappe_perm_parser.frappe_orm_helper.frappe_query_builder_helper import (  # noqa: E501 isort: skip
    get_linked_doc_references,
    append_query_condition,
)


class QueryBuilderStrategy:
    def get_doctype_list(self, qb=None):

        return get_linked_doctypes(qb.get_sql())

    def append_field_level_control(self, qb=None, doctypes=[], user=None):
        user_permission_hash = get_user_permissions_as_hash(user, doctypes)

        if len(user_permission_hash[PermHash.all]) > 0:
            for key, value in user_permission_hash[PermHash.all].items():
                # Remove doctype reference which dont have a table
                # reference in provided query
                doctype_references = get_linked_doc_references(key, doctypes)

                for doc, values in doctype_references.items():
                    # For cases where a doctype has self as well as
                    # all permissions they should be excluded when
                    # doc reference is a part of all.
                    self_perm = user_permission_hash[PermHash.self].get(doc)
                    final_value = value if not self_perm else value + self_perm

                    qb = append_query_condition(
                        qb,
                        doc,
                        {
                            "fieldname": values.get("fieldname")[0],
                            "value": final_value,
                        },  # noqa: E501
                    )

        if len(user_permission_hash[PermHash.specific]) > 0:
            for key, value in user_permission_hash[PermHash.specific].items():
                doctype_references = get_linked_doc_references(key, doctypes)

                for perms in value:
                    qb = append_query_condition(
                        qb,
                        perms.get("applicable_for"),
                        {
                            "fieldname": doctype_references[
                                perms.get("applicable_for")
                            ].get("fieldname"),
                            "value": [perms.get("doc")],
                        },
                    )

        if len(user_permission_hash[PermHash.self]) > 0:
            # Remove doctype reference which dont have a table
            # reference in provided query

            for doc in doctypes:
                if doc in user_permission_hash[PermHash.self]:
                    # For cases where a doctype has self as well
                    # as all permissions they should be excluded
                    # when doc reference is a part of all.
                    if not user_permission_hash[PermHash.all].get(doc):
                        qb = append_query_condition(
                            qb,
                            doc,
                            {
                                "fieldname": "name",
                                "value": user_permission_hash[
                                    PermHash.self
                                ].get(  # noqa: E501
                                    doc
                                ),  # noqa: E501
                            },
                        )

        return qb

    def append_global_level_control(self, qb=None, doctypes=[], user=None):

        return qb
