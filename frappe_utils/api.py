import json

import frappe

from frappe_utils.auth import get_decoded_base64_object
from frappe_utils.commands.site import execute_sync_masters
from frappe_utils.common.util import get_formatted_secret, get_secret_options
from frappe_utils.override import CustomDatabaseQuery


@frappe.whitelist()
def custom_resource_list(
    doctype,
    fields=None,
    filters=None,
    group_by=None,
    order_by=None,
    limit_start=None,
    limit_page_length=None,
    parent=None,
    debug=False,
    as_dict=True,
    or_filters=None,
    # NOTE: encoded string for an empty object
    overwrite_options="e30=",
    *args,
    **kwargs,
):
    kwargs = frappe._dict(
        doctype=doctype,
        parent_doctype=kwargs.get("parent_doctype", parent),
        fields=fields,
        filters=filters,
        or_filters=or_filters,
        group_by=group_by,
        order_by=order_by,
        limit_start=limit_start,
        limit_page_length=limit_page_length,
        debug=debug,
        as_list=not as_dict,
    )

    del kwargs["doctype"]

    options = get_decoded_base64_object(
        overwrite_options,
        "Failed to decode 'overwrite_options' please ensure"
        " you have provided stringified base64 encoded options",
    )
    return CustomDatabaseQuery(
        doctype=doctype, overwrite_options=options, fields=fields  # noqa: E501
    ).execute(*args, **kwargs)


@frappe.whitelist(methods=["POST"])
def sync_masters_api(master_doctype=[], force=False):
    master_doctype = json.loads(master_doctype)
    frappe.only_for("System Manager")

    if force:
        # only allow Administrator to force master sync
        if frappe.session.user != "Administrator":
            frappe.throw("This action is only allowed for Administrator")

        if not master_doctype:
            frappe.throw("Force sync needs Master Doctype name")

    frappe.enqueue(
        execute_sync_masters, master_doctype=master_doctype, force=force
    )  # noqa: 501

    return


@frappe.whitelist(methods=["GET"])
def get_decrypted_secret(doctype, docname, fieldname, complete_decode=False):
    field_meta = frappe.get_meta(doctype).get_field(fieldname).as_dict()
    options = get_secret_options(field_meta)

    if options.roles:
        frappe.only_for(options.roles)

    # get doc and doctype meta
    doc = frappe.get_doc(doctype, docname)

    if field_meta.fieldtype != "Password":
        frappe.throw("Non Password fields cannot be decrypted.")

    if not field_meta.options:
        frappe.throw("No Decode options provided for requested field.")

    # get secret options and decoded secret
    decoded_secret = doc.get_password(fieldname)

    # allow complete decode if specified
    if complete_decode:
        if options.allow_decode:
            return decoded_secret
        else:
            frappe.throw("Complete decode is not allowed for requested field.")

    # return formatted secret
    return get_formatted_secret(options.format, decoded_secret)
