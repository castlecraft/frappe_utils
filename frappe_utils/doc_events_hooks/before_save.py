from frappe_utils.common.decorators import exclude_doc_event_hooks

from frappe_utils.composite_key.generate_composite_key import (  # noqa: 501 isort:skip
    execute_composite_key,
)


@exclude_doc_event_hooks()
def before_save(self, method=None):
    if method is None:
        return True

    execute_composite_key(self)
