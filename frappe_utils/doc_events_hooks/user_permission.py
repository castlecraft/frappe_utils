import frappe

from frappe_utils.common.decorators import exclude_doc_event_hooks


@exclude_doc_event_hooks()
def before_validate(self, method=None):

    if method is None:
        return True

    if self.is_new():
        return

    if self.custom_has_permission_rule:
        frappe.throw("Doc with linked Permission Rule cannot be updated")
