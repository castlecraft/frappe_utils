import frappe

from frappe_utils.composite_key.generate_composite_key import (  # noqa: 501 isort:skip
    generate_composite_key,
)


def autoname(self, method):
    if self.doctype != "Composite Key" and frappe.db.exists(
        "Composite Key", self.doctype
    ):
        doc = frappe.get_cached_doc("Composite Key", self.doctype)
        if doc.type == "In Primary ID":
            self.name = generate_composite_key(self, doc)
