import json

import frappe
from frappe import _

from frappe.model.db_query import (  # isort: skip noqa: 501
    DatabaseQuery,
    has_any_user_permission_for_doctype,
    requires_owner_constraint,
)


class CustomDatabaseQuery(DatabaseQuery):
    def __init__(self, overwrite_options, fields, doctype):
        self.fr_util_overwrite_options = overwrite_options

        self.fr_util_overwrite_fields = fields

        if (isinstance(fields, str) and fields == "*") or (
            isinstance(fields, list | tuple)
            and len(fields) == 1
            and fields[0] == "*"  # noqa: 501
        ):
            self.fr_util_overwrite_fields = frappe.db.get_table_columns(
                doctype
            )  # noqa: 501

        super().__init__(doctype)

    def prepare_args(self):
        args = super().prepare_args()

        args.fields = self.fr_util_overwrite_fields

        if isinstance(args.fields, str):
            args.fields = json.loads(args.fields)

        args.fields = ", ".join(args.fields)

        return args

    def build_match_conditions(self, as_condition=True) -> str | list:
        """add match conditions if applicable"""

        if not self.fr_util_overwrite_options:
            return super().build_match_conditions(as_condition=as_condition)

        self.match_filters = []
        self.match_conditions = []
        only_if_shared = False
        if not self.user:
            self.user = frappe.session.user

        if not self.tables:
            self.extract_tables()

        role_permissions = frappe.permissions.get_role_permissions(
            self.doctype_meta, user=self.user
        )

        # NOTE: Following value is set to empty string for avoiding false
        # 		condition mapping that causes frappe db query to fail
        # 		If shared is set to [] it will lead to `No permission
        # 		to read` errors
        self.shared = [""]
        if not self.fr_util_overwrite_options.get("exclude_shared_doc"):
            self.shared = frappe.share.get_shared(self.doctype, self.user)

        if (
            not self.doctype_meta.istable
            and not (
                role_permissions.get("select")
                or role_permissions.get("read")  # noqa: E501
            )
            and not self.flags.ignore_permissions
            and not has_any_user_permission_for_doctype(
                self.doctype, self.user, self.reference_doctype
            )
        ):
            only_if_shared = True
            if not self.shared:
                frappe.throw(
                    _("No permission to read {0}").format(_(self.doctype)),
                    frappe.PermissionError,
                )
            else:
                self.conditions.append(self.get_share_condition())

        else:
            # skip user perm check if owner constraint is required
            if requires_owner_constraint(role_permissions):
                self.match_conditions.append(
                    f"`tab{self.doctype}`.`owner` = {frappe.db.escape(self.user, percent=False)}"  # noqa: E501
                )

                # add user permission only if role has read perm
            elif role_permissions.get("read") or role_permissions.get(
                "select"
            ):  # noqa: E501
                # get user permissions
                user_permissions = frappe.permissions.get_user_permissions(
                    self.user
                )  # noqa: E501  # noqa: E501
                self.add_user_permissions(user_permissions)

        if as_condition:
            conditions = ""
            if self.match_conditions:
                # will turn out like ((blog_post in (..) and
                # blogger in (...)) or (blog_category in (...)))
                conditions = (
                    "((" + ") or (".join(self.match_conditions) + "))"  # noqa: E501
                )  # noqa: E501

            doctype_conditions = self.get_permission_query_conditions()
            if doctype_conditions:
                conditions += (
                    (" and " + doctype_conditions)
                    if conditions
                    else doctype_conditions  # noqa: E501
                )

                # share is an OR condition, if there is a role permission
            if not only_if_shared and self.shared and conditions:
                conditions = (
                    f"(({conditions}) or ({self.get_share_condition()}))"  # noqa: E501
                )
            return conditions

        else:
            return self.match_filters
