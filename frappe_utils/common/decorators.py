from functools import wraps
from typing import Callable

import frappe
from frappe.utils.caching import __generate_request_cache_key

from frappe_utils.common.pre_hook_validate import should_skip_hook


def exclude_doc_event_hooks(in_migrate=True):
    def decorator(func):
        def wrapper(self, method=None):
            if in_migrate and should_skip_hook():
                return True

            return func(self, method)

        return wrapper

    return decorator


def should_cache(config):
    return not config.get("frappe_utils_disable_cache") if config else True


def get_config(val):
    if isinstance(val, tuple):
        config = val[-1]
        if isinstance(config, dict) and "frappe_utils_disable_cache" in config:
            return config, True

    config = None
    return config, False


def utils_redis_cache(
    ttl: int | None = 3600, user: str | bool | None = None
) -> Callable:
    """Decorator to cache method calls and its return values in Redis

    args:
            ttl: time to expiry in seconds, defaults to 1 hour
            user: `true` should cache be specific to session user.
    """

    def wrapper(func: Callable = None) -> Callable:
        func_key = f"{func.__module__}.{func.__qualname__}"

        def clear_cache():
            frappe.cache().delete_keys(func_key)

        func.clear_cache = clear_cache
        func.ttl = ttl if not callable(ttl) else 3600

        @wraps(func)
        def redis_cache_wrapper(*args, **kwargs):
            func_call_key = func_key + str(
                __generate_request_cache_key(args, kwargs)
            )  # noqa: E501
            if frappe.cache().exists(func_call_key):
                return frappe.cache().get_value(func_call_key, user=user)
            else:
                val = func(*args, **kwargs)
                ttl = getattr(func, "ttl", 3600)
                config, config_in_val = get_config(val)

                if config_in_val:
                    # Removing config from value.
                    val = val[:-1]
                    # If tuple contain only one element then don't return tuple
                    if len(val) == 1:
                        val = val[0]

                if should_cache(config):
                    frappe.cache().set_value(
                        func_call_key, val, expires_in_sec=ttl, user=user
                    )
                    return val

                return val

        return redis_cache_wrapper

    if callable(ttl):
        return wrapper(ttl)
    return wrapper
