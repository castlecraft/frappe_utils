import frappe


def get_user_permissions(user=None):
    """Get all users permissions for the user as a dict of doctype"""

    if not user:
        user = frappe.session.user

    if not user or user in ("Administrator", "Guest"):
        return {}

    # TODO: Think of a way we can cache it by hooking core function
    # cached_user_permissions = frappe.cache().hget(
    #     "custom_granular_permission_user_permissions", user
    # )

    # if cached_user_permissions is not None:
    #     return cached_user_permissions

    out = {}

    def add_doc_to_perm(perm, doc_name, is_default):
        # group rules for each type
        # for example if allow is "Customer", then build all allowed customers
        # in a list
        # pop is_default as its getting value from add_doc_to_perm params
        if perm.get("is_default"):
            perm.pop("is_default")

        if not out.get(perm.allow):
            out[perm.allow] = []

        out[perm.allow].append(
            frappe._dict(
                {
                    "doc": doc_name,
                    "applicable_for": perm.get("applicable_for"),
                    "is_default": is_default,
                    **perm,
                }
            )
        )

    try:
        for perm in frappe.get_all(
            "User Permission",
            fields=[
                "allow",
                "for_value",
                "applicable_for",
                "is_default",
                "hide_descendants",
                "custom_granular_permission_check",
                "custom_granular_permission_select",
                "custom_granular_permission_read",
                "custom_granular_permission_write",
                "custom_granular_permission_create",
                "custom_granular_permission_delete",
                "custom_granular_permission_submit",
                "custom_granular_permission_cancel",
                "custom_granular_permission_amend",
            ],
            filters=dict(user=user),
        ):

            meta = frappe.get_meta(perm.allow)
            add_doc_to_perm(perm, perm.for_value, perm.is_default)

            if meta.is_nested_set() and not perm.hide_descendants:
                descendants = frappe.db.get_descendants(
                    perm.allow, perm.for_value
                )  # noqa: 501
                for doc in descendants:
                    add_doc_to_perm(perm, doc, False)

        out = frappe._dict(out)
        # frappe.cache().hset(
        #     "custom_granular_permission_user_permissions", user, out
        # )  # noqa: E501
    except frappe.db.SQLError as e:
        if frappe.db.is_table_missing(e):
            # called from patch
            pass

    return out
