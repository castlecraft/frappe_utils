import frappe
from frappe.utils.background_jobs import get_queue, get_queue_list


@frappe.whitelist()
def remove_enqueued_jobs(queues):
    """
    Removes all jobs from specified queues
    """

    q = get_queue(queues)
    jobs = q.get_jobs()

    if not jobs:
        frappe.throw(f"There are no jobs in the '{queues}' queue to remove")

    q.delete()


@frappe.whitelist()
def get_all_queue():
    return get_queue_list()
