import frappe
from frappe.desk.reportview import get_count as get_filtered_count
from frappe.desk.reportview import get_form_params
from frappe.utils import deprecated

from frappe_utils.api import custom_resource_list


@frappe.whitelist()
@deprecated
def get_paginated_list():
    """
    NOTE:
    Following API is deprecated,
    kindly switch to `frappe_utils.api.custom_resource_list`
    """

    args = get_form_params()

    form = frappe.local.form_dict

    args["fields"] = form.get("fields", ["name"])

    docs = custom_resource_list(**args)

    return {
        "docs": docs,
        "length": get_filtered_count(),
    }
