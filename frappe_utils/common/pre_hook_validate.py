import frappe


def should_skip_hook():
    if frappe.flags.in_migrate:
        return True
