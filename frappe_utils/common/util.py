import json
import os
import secrets

import frappe
from frappe.modules.import_file import import_file_by_path


def deep_merge(a, b, path=[]):
    for key in b:
        if key in a:
            if isinstance(a[key], dict) and isinstance(b[key], dict):
                deep_merge(a[key], b[key], path + [str(key)])
            elif type(a[key]) is type(b[key]):
                a[key] = b[key]
            elif a[key] != b[key]:
                raise Exception("Conflict at " + ".".join(path + [str(key)]))
        else:
            a[key] = b[key]
    return a


def sync_json_to_schema(json):
    """
    - create a temp meta json file
    - provide it to frappe's import_file_by_path
    - remove meta json file once imported
    """

    temp_path = f"/tmp/meta-{secrets.token_hex(3)}.json"

    with open(temp_path, "w") as outfile:
        outfile.write(frappe.as_json(json))

    is_imported = import_file_by_path(
        temp_path, data_import=True, force=True, reset_permissions=True
    )

    if is_imported:
        frappe.db.commit()

    os.remove(temp_path)


@frappe.whitelist()
def get_master_meta_doctype():
    from frappe_utils.commands.site import load_json_file

    masters_meta = []
    for app in frappe.get_installed_apps():
        masters_path = frappe.get_app_path(app, "masters")

        if os.path.exists(masters_path):
            masters_files = os.listdir(masters_path)
            for file in masters_files:
                doctype_folder_name = frappe.get_app_path(app, "masters", file)
                data_json_path = os.path.join(doctype_folder_name, "data.json")
                data_json = load_json_file(data_json_path)
                if data_json:
                    doctype_name = data_json[0].get("doctype")
                    masters_meta.append(doctype_name)

    return masters_meta


def get_secret_options(field_meta):
    try:
        return frappe._dict(json.loads(field_meta.options))
    except Exception:
        return frappe.throw(
            "Failed to decode JSON for provided password options, "
            "please provide valid options"
        )


def get_formatted_secret(secret_format, decoded_secret):
    # Split the format into parts
    parts = secret_format.split()

    # Calculate the number of visible digits
    visible_digits = sum(part.count("#") for part in parts)

    # Get the last 'visible_digits' of the decoded secret
    visible_part = (
        decoded_secret[-visible_digits:]
        if len(decoded_secret) >= visible_digits
        else decoded_secret.rjust(visible_digits, "*")
    )

    # Initialize the result and the index for visible part
    result = []
    visible_index = 0

    # Process each part of the format
    for part in parts:
        new_part = ""
        for char in part:
            if char == "*":
                new_part += "*"
            elif char == "#":
                if visible_index < len(visible_part):
                    new_part += visible_part[visible_index]
                    visible_index += 1
                else:
                    new_part += "*"
            else:
                new_part += char
        result.append(new_part)

    # Join the parts and return
    return " ".join(result)
