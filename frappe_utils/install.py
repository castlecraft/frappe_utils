def after_install():
    from frappe_utils.patches.create_user_access_control_workflow import (  # noqa: E501 # isort: skip
        create_workflow,
    )

    create_workflow()
