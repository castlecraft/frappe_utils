from frappe_utils.frappe_perm_parser.frappe_orm_helper.frappe_sql_helper import (  # noqa: 501 isort: skip
    get_linked_fields_and_doctypes_hash,
)

import frappe

GRANULAR_PERMISSION_TYPES = {
    "select": "granular_permission_select",
    "read": "granular_permission_read",
    "write": "granular_permission_write",
    "create": "granular_permission_create",
    "delete": "granular_permission_delete",
    "submit": "granular_permission_submit",
    "cancel": "granular_permission_cancel",
    "amend": "granular_permission_amend",
}


def validate_perms(doc, user=None, ptype="read"):
    if is_granular_permission_disabled():
        return True

    if not ptype:
        return

    permission_rules = get_permission_rules(doc.doctype, user=user)

    # return if no Permission Rule created
    if not len(permission_rules):
        return True

    link_fields = get_linked_fields_and_doctypes_hash(doc.meta)

    # temp fix
    # remove self doctype if doc is submittable
    # will need to ignore this in case of amend
    if link_fields.get(doc.doctype) and ptype != "amend":
        link_fields.pop(doc.doctype)

    for rule in permission_rules:
        if rule.get(GRANULAR_PERMISSION_TYPES.get(ptype)) == 1:

            is_all_rules_valid = validate_combination_permissions(
                rule.permission_rule_links, doc, link_fields
            )

            # return True if valid combinations are found,
            # else check in next iteration
            if is_all_rules_valid:
                return True

    # If no valid rule combinations are found
    return False


def validate_combination_permissions(permission_rule_links, doc, link_fields):
    # Ensure all linked fields in the doc
    # match the corresponding links in the permission_rule_links

    return all(
        doc.get(link_fields.get(link.link_doctype)) == link.link_name
        for link in permission_rule_links
        if doc.get(link_fields.get(link.link_doctype))
    )


def get_perm_rule_hash_map(permission_rules):
    hash_map = {
        "select": set(),
        "read": set(),
        "write": set(),
        "create": set(),
        "delete": set(),
        "submit": set(),
        "cancel": set(),
        "amend": set(),
    }

    for permission_rule in permission_rules:
        # get all Permission Rule Link child table config
        # linked in Permission Rule DocType
        rules = frappe.get_all(
            "Permission Rule Link",
            filters=[["parent", "in", permission_rule.name]],
            fields=["link_doctype", "link_name"],
        )

        for rule in rules:
            if permission_rule.granular_permission_select:
                hash_map["select"].add(
                    (rule["link_doctype"], rule["link_name"])
                )  # noqa: 501

            if permission_rule.granular_permission_read:
                hash_map["read"].add(
                    (rule["link_doctype"], rule["link_name"])
                )  # noqa: 501

            if permission_rule.granular_permission_write:
                hash_map["write"].add(
                    (rule["link_doctype"], rule["link_name"])
                )  # noqa: 501

            if permission_rule.granular_permission_create:
                hash_map["create"].add(
                    (rule["link_doctype"], rule["link_name"])
                )  # noqa: 501

            if permission_rule.granular_permission_delete:
                hash_map["delete"].add(
                    (rule["link_doctype"], rule["link_name"])
                )  # noqa: 501

            if permission_rule.granular_permission_submit:
                hash_map["submit"].add(
                    (rule["link_doctype"], rule["link_name"])
                )  # noqa: 501

            if permission_rule.granular_permission_cancel:
                hash_map["cancel"].add(
                    (rule["link_doctype"], rule["link_name"])
                )  # noqa: 501

            # need to be fixed
            if permission_rule.granular_permission_amend:
                hash_map["amend"].add(
                    (rule["link_doctype"], rule["link_name"])
                )  # noqa: 501

    return hash_map


def get_permission_rules(doctype, user=None):
    user = user or frappe.session.user
    # get all User Permission Rule names
    # based on session user and current doctype
    user_permission_rules = frappe.get_all(
        "User Permission Rule",
        filters=[
            ["user", "=", user],
            ["for_doctype", "=", doctype],
        ],  # noqa: 501
        pluck="name",
    )

    # get User Permission Rule Link child table
    # linked to User Permission Rule
    user_permission_rule_links = frappe.get_all(
        "User Permission Rule Link",
        filters=[["parent", "in", user_permission_rules]],
        pluck="permission_rule",
    )

    # get all granular permissions config from User Permission
    # linked in User Permission Rule
    permission_rules = frappe.get_all(
        "Permission Rule",
        filters=[
            ["name", "in", user_permission_rule_links],
            ["granular_permission_check", "=", 1],
        ],
        fields=[
            "name",
            "granular_permission_check",
            "granular_permission_select",
            "granular_permission_read",
            "granular_permission_write",
            "granular_permission_create",
            "granular_permission_delete",
            "granular_permission_submit",
            "granular_permission_cancel",
            "granular_permission_amend",
            "`tabPermission Rule Link`.link_doctype",
            "`tabPermission Rule Link`.link_name",
        ],
    )

    return group_permission_rule_links(permission_rules)


def group_permission_rule_links(perm_rules):
    # Dictionary to hold grouped data
    grouped_perm_rules = {}

    for rule in perm_rules:
        name = rule.name
        if name not in grouped_perm_rules:
            # Create a new entry if this name hasn't been seen before
            grouped_perm_rules[name] = frappe._dict(
                {
                    key: value
                    for key, value in rule.items()
                    if key not in ["link_doctype", "link_name"]
                }
            )
            grouped_perm_rules[name]["permission_rule_links"] = []

        # Add the link information to permission_rule_links
        grouped_perm_rules[name]["permission_rule_links"].append(
            frappe._dict(
                {
                    "link_doctype": rule.link_doctype,
                    "link_name": rule.link_name,
                }  # noqa: 501
            )
        )

    # Convert the grouped data dictionary to a list
    return list(grouped_perm_rules.values())


def has_permission(doc, user=None, ptype=None):
    return validate_perms(
        doc=doc,
        user=user or frappe.session.user,
        ptype=ptype,
    )


def get_permission_query_conditions(user, doctype):
    if is_granular_permission_disabled():
        return ""

    user = user or frappe.session.user

    permission_rules = get_permission_rules(doctype, user=user)
    or_conditions = []

    # return if there's no permission_rules created
    if not permission_rules:
        return ""

    link_fields = get_linked_fields_and_doctypes_hash(frappe.get_meta(doctype))

    for rule in permission_rules:
        and_condition = []
        for links in rule.permission_rule_links:
            and_condition.append(
                f"""`tab{doctype}`.`{link_fields.get(links.link_doctype)}` = '{links.link_name}'"""  # noqa: 501
            )

        or_conditions.append(f"({' AND '.join(and_condition)})")

    conditions = " OR ".join(or_conditions)

    if not conditions:
        conditions = "0"

    return f"""({conditions})""" if conditions else ""


def is_granular_permission_disabled():
    return frappe.get_cached_value(
        "Frappe Util Settings", None, "disable_granular_permission"
    )
