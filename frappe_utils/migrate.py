import frappe
from frappe.migrate import SiteMigration, atomic
from frappe.modules.utils import sync_customizations
from frappe.utils.dashboard import sync_dashboards
from frappe.utils.fixtures import sync_fixtures

from frappe.modules.patch_handler import (  # isort: skip
    PatchError,
    PatchType,
    get_patches_from_app,
    run_single,
)


def run_patch(patch, skip_failing=None):
    try:
        if not run_single(patchmodule=patch):
            print(patch + ": failed: STOPPED")  # noqa: T201, T001
            raise PatchError(patch)
    except Exception:
        if not skip_failing:
            raise
        else:
            print("Failed to execute patch")  # noqa: T201, T001


class PartialSiteMigration(SiteMigration):
    @atomic
    def run_schema_updates_for_app(self, app):
        """Run patches for app as defined in patches.txt,
        sync schema changes as defined in the {doctype}.json files
        """

        executed = set(
            frappe.get_all("Patch Log", fields="patch", pluck="patch")
        )  # noqa: E501
        pre_model_sync_patches = get_patches_from_app(
            app, patch_type=PatchType.pre_model_sync
        )
        post_model_sync_patches = get_patches_from_app(
            app, patch_type=PatchType.post_model_sync
        )

        for patch in pre_model_sync_patches:
            if patch and (patch not in executed):
                run_patch(patch)

        frappe.model.sync.sync_for(app)

        for patch in post_model_sync_patches:
            if patch and (patch not in executed):
                run_patch(patch)

        # patches to be run in the end
        if frappe.flags.final_patches:
            for patch in frappe.flags.final_patches:
                patch = patch.replace("finally:", "")
                run_patch(patch)

    @atomic
    def post_schema_updates_for_app(self, app: str):
        """Execute pending migration tasks post patches execution & schema sync
        This includes:
        * Sync `Scheduled Job Type` and scheduler events defined in hooks
        * Sync fixtures & custom scripts
        * Sync in-Desk Module Dashboards
        * Sync customizations: Custom Fields, Property Setters, Custom Permissions
        * Sync Frappe's internal language master
        * Flush deferred inserts made during maintenance mode.
        * Sync Portal Menu Items
        * Sync Installed Applications Version History
        * Execute `after_migrate` hooks
        """  # noqa: E501
        sync_fixtures(app=app)
        sync_dashboards(app=app)
        sync_customizations(app=app)

        frappe.get_single("Portal Settings").sync_menu()
        frappe.get_single("Installed Applications").update_versions()

        for fn in frappe.get_hooks("after_migrate", app_name=app):
            frappe.get_attr(fn)()

    @atomic
    def pre_schema_updates_for_app(self, app):
        """Executes `before_migrate` hooks"""
        for fn in frappe.get_hooks("before_migrate", app_name=app):
            frappe.get_attr(fn)()

    def run_app(self, site: str, app: str):
        """Run Migrate operation for app on site specified.
        This method initializes and destroys connections to the site database.
        """
        if not self.required_services_running():
            raise SystemExit(1)

        self.setUp()
        try:
            self.pre_schema_updates_for_app(app=app)
            self.run_schema_updates_for_app(app=app)
            self.post_schema_updates_for_app(app=app)
        finally:
            self.tearDown()
