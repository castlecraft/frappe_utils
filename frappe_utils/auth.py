import base64
import json

import frappe
from frappe import _
from frappe.oauth import generate_json_error_response

from frappe_utils.custom_auth_case import CustomAuthHttpCase


@frappe.whitelist(allow_guest=True)
def validate():
    """
    Additional validation to execute along with frappe request
    """
    authorization_header = frappe.get_request_header(
        "X-FRAPPE-CUSTOM-HEADER", ""
    )  # noqa: E501

    if authorization_header:
        provider = frappe.get_cached_doc(
            "Custom Provider", authorization_header
        )  # noqa: 501

        if provider.enable:
            # Validate Referer
            return (
                execute_custom_provider(provider)
                if validate_custom_provider(provider)
                else None
            )

        else:
            return throw_invalid_request(_("Frappe provider disabled."))

    else:
        return throw_invalid_request(
            _(
                "No expected header found, please add appropriate value"
                "for 'X-FRAPPE-CUSTOM-HEADER'"
            )
        )


def execute_custom_provider(provider):
    # Append Cookies
    cookies = map_provider_cookies(provider)

    # Append header
    headers = map_provider_token(provider)

    # Make provider authorization request
    response = CustomAuthHttpCase().get_token_response(
        method=provider.method,
        url=provider.url,
        headers=headers,
        cookies=cookies,
        verify=not bool(provider.bypass_tls_validate),
    )

    # Map user email from response with provider mapping
    user = map_user_email_from_response(provider, response)

    if not user:
        return throw_invalid_request(
            _(
                "Failed to parse email, "
                "please check mapping for email response."  # noqa: 501
            )
        )

    # Check if email from response exists on frappe
    user_exists = frappe.db.exists("User", user)

    # Create user if configured
    if user_exists:
        return generate_user_token(user, provider.oauth_client, provider)
    elif provider.create_user_flag:
        user = create_new_user(provider, response, user)

        if user:
            return generate_user_token(user, provider.oauth_client, provider)

    else:
        return throw_invalid_request(
            _(
                "No user found for provided email,"
                "please add user or enable new user creation."
            )
        )


def validate_custom_provider(provider):
    request_referer = frappe.get_request_header("Referer", "")

    # Validate referer
    if request_referer != provider.referer:
        throw_invalid_request(
            _("Found invalid referer: ${0}").format(request_referer)
        )  # noqa: 501
        return False

    # Validate Custom Provider Header
    if provider.header == "Custom":
        custom_value = frappe.get_request_header(provider.value, "")

        if not custom_value:
            throw_invalid_request(
                _(
                    "Please provide encoded header or cookie"
                    " in {}, no value found.".format(provider.value)
                )
            )
            return False

        # validate base64 object parsing
        decoded_value = get_decoded_base64_object(custom_value)

        if not decoded_value.get("cookie") and not decoded_value.get("header"):
            throw_invalid_request(
                _(
                    "No header or cookie found in decoded 'X-FRAPPE"
                    "-CUSTOM-VALUE'. Please provide either of them."
                )
            )
            return False

    return True


def map_user_email_from_response(provider, response):
    # Map user email from response
    email = ""

    try:
        email = get_data(
            payload=response,
            param=provider.get("email_map"),
        )  # noqa: 501

    except Exception as e:
        frappe.log_error(
            title="frappe_utils_auth_response_failed_email_parsing",
            message=e,
        )

    if not email and provider.get("non_email_user_flag"):
        email = generate_non_user_email(provider, response)

    return email


def generate_non_user_email(provider, response):
    prefix = provider.get("prefix")
    suffix = provider.get("suffix")
    separator = provider.get("separator")
    domain = provider.get("domain")

    if not prefix or not suffix:
        frappe.throw(
            "Failed to generate non user email, prefix or suffix not found."  # noqa: 501
        )

    if not get_data(response, prefix) or not get_data(response, prefix):
        frappe.throw(
            "Failed to generate non user email, mapping for prefix or suffix found empty."  # noqa: 501
        )

    return "{}{}{}@{}".format(
        get_data(response, prefix),
        separator,
        get_data(response, suffix),
        domain,  # noqa: 501
    )


def map_provider_cookies(provider):
    # Append cookie
    if provider.header == "Cookie":
        cookie = frappe.request.cookies.get(provider.value, "")

        if not cookie:
            frappe.throw(
                "No cookie found, requested provider "
                "needs to have a cookie '{}'.".format(provider.value)
            )

        return {provider.value: cookie}

    elif provider.header == "Custom":
        cookie = frappe.request.cookies.get(provider.value, "")

        if cookie:
            return get_decoded_base64_object(cookie).get("cookie", "")

    return {}


def map_provider_token(provider):
    # Append header
    if provider.header == "Token":
        token_header = frappe.get_request_header(provider.value, "")  # noqa: 501

        if not token_header:
            frappe.throw(
                "No token header found, "
                "requested provider needs to "
                "have a token {}.".format(provider.value)
            )

        return {provider.value: token_header}

    elif provider.header == "Custom":
        token_header = frappe.get_request_header(provider.value, "")  # noqa: 501

        if token_header:
            return get_decoded_base64_object(token_header).get("header", {})

    return {}


def get_decoded_base64_object(encoded_str, message=None):
    try:
        decoded_str = base64.b64decode(encoded_str).decode()
        return json.loads(decoded_str)
    except Exception:
        frappe.throw(
            message
            if message
            else _(
                "Failed to decode 'X-FRAPPE-CUSTOM-VALUE'. Please ensure that the provided input is a valid base64-encoded JSON string."  # noqa: 501
            )
        )


def create_new_user(provider, response, user_email):
    user_name = (
        get_data(payload=response, param=provider.name_map)
        if provider.name_map
        else user_email
    )

    if not user_name:
        throw_invalid_request(
            _(
                "Failed to parse name, "
                "please check mapping for name response."  # noqa: 501
            )
        )
        return

    # Create User
    user_doc = frappe.get_doc(
        {
            "doctype": "User",
            "email": user_email,
            "send_welcome_email": 0,
        }
    )

    user_doc.first_name = user_name

    for role in provider.roles:
        user_doc.append("roles", {"role": role.role})

    user_doc.insert(ignore_permissions=True)

    permission_rules = provider.permission_rules
    if permission_rules:
        create_perm_rule(permission_rules, response, user_doc)

    user_mapping = provider.user_mapping
    if user_mapping:
        for user in user_mapping:
            field_name = user.field_name
            field_value = get_eval(payload=response, param=user.field_value)
            if not field_value:
                throw_invalid_request(
                    _(
                        f"Failed to parse field_value '{user.field_value}' for field_name '{user.field_name}', "  # noqa: 501
                        f"please check mapping for field_value response. "
                    )
                )
                return
            user_doc.set(field_name, str(field_value))

        user_doc.save(ignore_permissions=True)

    return user_doc


def generate_user_token(user, oauth_client, provider):
    # create oauth_bearer_token
    token_doc = frappe.new_doc("OAuth Bearer Token")
    token_doc.user = user
    token_doc.client = oauth_client
    token_doc.access_token = frappe.generate_hash(length=30)
    token_doc.refresh_token = frappe.generate_hash(length=30)
    token_doc.expires_in = 3600
    token_doc.scopes = "openid all"
    token_doc.insert(ignore_permissions=True)

    token_dict = frappe._dict(
        {
            "access_token": token_doc.access_token,
            "token_type": "Bearer",
            "expires_in": token_doc.expires_in,
            "refresh_token": token_doc.refresh_token,
            "email": user,
        }
    )

    if provider.redirect_url:
        # Convert token dictionary to JSON string
        token_json = json.dumps(token_dict)

        # Encode the JSON string in base64
        encoded_token = base64.b64encode(token_json.encode("utf-8")).decode(
            "utf-8"
        )  # noqa: E501

        # Construct the redirect URL with base64 encoded token
        redirect_url = f"{provider.redirect_url}?token={encoded_token}"

        # Perform the redirection
        frappe.local.response["type"] = "redirect"
        frappe.local.response["location"] = redirect_url
    else:
        frappe.local.response = token_dict

    if provider.create_session:
        frappe.local.login_manager.user = (
            user.email
            if isinstance(user, frappe.core.doctype.user.user.User)
            else user  # noqa: E501
        )
        frappe.local.login_manager.post_login()

    frappe.db.commit()


def create_perm_rule(permission_rules, response, user_doc):
    rules_dict = {}
    for rule in permission_rules:
        condition = rule.condition
        if get_eval(payload=response, param=condition):
            for_doctype = frappe.get_value(
                "Permission Rule",
                {"name": rule.permission_rule},
                "for_doctype",
            )  # noqa: 501

            # Check if for_doctype is already in my_dict, else create new entry
            if for_doctype not in rules_dict:
                rules_dict[for_doctype] = []
            rules_dict[for_doctype].append(rule)

    for for_doctype, rules in rules_dict.items():
        # Create User Permission Rule
        user_perm_rule_doc = frappe.get_doc(
            {
                "doctype": "User Permission Rule",
                "user": user_doc.name,
                "for_doctype": for_doctype,
            }
        )
        for perm_rule in rules:
            user_perm_rule_doc.append(
                "permission_rules",
                {
                    "permission_rule": perm_rule.permission_rule,
                    "condition": perm_rule.condition,
                },
            )  # noqa: 501

        user_perm_rule_doc.insert(ignore_permissions=True)


def get_data(payload, param):
    param = param.split(".")
    result = payload
    if param:
        for key in param:
            result = result.get(key, {})
        return result
    else:
        return None


def get_eval(payload, param):
    payload = frappe._dict(payload)
    return eval(param)


def throw_invalid_request(
    description="", status_code=400, error="invalid_request"
):  # noqa: 501
    e = frappe._dict({})
    e["description"] = description
    e["status_code"] = status_code
    e["error"] = error

    generate_json_error_response(e)
