import requests

from frappe_utils.process_response import process_response


class CustomAuthHttpCase:
    def get_token_response(self, method, url, headers, cookies, verify=False):
        return getattr(self, method.lower())(url, headers, cookies, verify)

    def get(self, url, headers, cookies, verify=False):
        response = process_response(
            requests.get(url, headers=headers, cookies=cookies, verify=verify)
        )  # noqa: 501
        return response

    def post(self, url, headers, cookies, verify=False):
        response = process_response(
            requests.post(url, headers=headers, cookies=cookies, verify=verify)
        )
        return response

    def put(self, url, headers, cookies, verify=False):
        response = process_response(
            requests.put(
                url=url, headers=headers, cookies=cookies, verify=verify
            )  # noqa: E501
        )
        return response
