import frappe


def execute_composite_key(self):
    if self.doctype != "Composite Key" and frappe.db.exists(
        "Composite Key", self.doctype
    ):
        doc = frappe.get_cached_doc("Composite Key", self.doctype)

        if doc.type == "In Doctype Field":
            composite_key_value = generate_composite_key(self, doc)
            setattr(self, doc.composite_key_field, composite_key_value)

        elif doc.type == "In Memory":
            validate_composite_key(self, doc)

        elif doc.type == "In Primary ID":
            set_primary_id(self, doc)


def validate_composite_key(self, doc):
    filters = {"name": ["!=", self.name]}

    for row in doc.composite_fields:
        filters[row.field_name] = self.get(row.field_name, None)

    existing_doc = frappe.db.exists(self.doctype, filters)

    if existing_doc:
        meesage_fields = ", ".join(
            [f"{key}: {value}" for key, value in filters.items()]
        )

        frappe.throw(
            "Document matching fields {fields} already exists in '{existing_doc}', please create a unique one.".format(  # noqa: E501
                fields=meesage_fields, existing_doc=existing_doc
            ),
        )


def generate_composite_key(self, doc):
    # Create composite key with empty values for missing fields
    composite_key_value = doc.key_separator.join(
        get_validated_composite_field(self, row)
        for row in doc.composite_fields  # noqa: E501
    )

    # Trim trailing separator if present
    if composite_key_value.endswith(doc.key_separator):
        composite_key_value = composite_key_value[
            : -len(doc.key_separator)
        ]  # noqa: 501

    return composite_key_value


def get_validated_composite_field(self, row):
    return (
        str(self.get(row.field_name))
        if (self.get(row.field_name))
        or (
            self.get(row.field_name) == 0
            or type(self.get(row.field_name)) == int  # noqa: E501
        )
        else ""
    )


def set_primary_id(self, doc):
    composite_key_value = generate_composite_key(self, doc)

    if not self.get("__islocal") and composite_key_value != self.name:
        frappe.rename_doc(doc.name, self.name, composite_key_value)
        self.name = composite_key_value
