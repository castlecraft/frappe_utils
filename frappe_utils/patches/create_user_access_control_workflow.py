import frappe


def execute():
    create_workflow()


def create_workflow():
    workflow_name = "User Access Control Commitment Process"

    # avoid duplicate error
    if frappe.db.exists("Workflow", workflow_name):
        return

    doc = frappe.new_doc("Workflow")
    doc.workflow_name = workflow_name
    doc.document_type = "User Access Control"
    doc.is_active = 1

    for state in get_workflow_states():
        doc.append("states", state)

    for transition in get_transitions():
        doc.append("transitions", transition)

    doc.insert()


def create_workflow_state(state):
    if not frappe.db.exists("Workflow State", state):
        frappe.get_doc(
            {"doctype": "Workflow State", "workflow_state_name": state}
        ).insert()

    return state


def get_workflow_states():

    states = [
        {
            "state": create_workflow_state("Pending"),
            "doc_status": "0",
            "allow_edit": "All",
        },
        {
            "state": create_workflow_state("Sent For Approval"),
            "doc_status": "0",
            "allow_edit": "All",
        },
        {
            "state": create_workflow_state("Approved"),
            "doc_status": "1",
            "allow_edit": "System Manager",
        },
        {
            "state": create_workflow_state("Rejected"),
            "doc_status": "0",
            "allow_edit": "All",
        },
    ]

    return states


def create_workflow_action_master(action):
    if not frappe.db.exists("Workflow Action Master", action):
        frappe.get_doc(
            {
                "doctype": "Workflow Action Master",
                "workflow_action_name": action,
            }  # noqa: 501
        ).insert()

    return action


def get_transitions():

    transitions = [
        {
            "state": create_workflow_state("Pending"),
            "action": create_workflow_action_master("Send For Approval"),
            "next_state": create_workflow_state("Sent For Approval"),
            "allowed": "All",
        },
        {
            "state": create_workflow_state("Sent For Approval"),
            "action": create_workflow_action_master("Approve"),
            "next_state": create_workflow_state("Approved"),
            "allowed": "System Manager",
        },
        {
            "state": create_workflow_state("Sent For Approval"),
            "action": create_workflow_action_master("Reject"),
            "next_state": create_workflow_state("Rejected"),
            "allowed": "System Manager",
        },
        {
            "state": create_workflow_state("Rejected"),
            "action": create_workflow_action_master("Amend Changes"),
            "next_state": create_workflow_state("Pending"),
            "allowed": "All",
        },
    ]

    return transitions
