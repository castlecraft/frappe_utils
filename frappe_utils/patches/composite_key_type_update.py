import frappe


def execute():
    composite_keys = frappe.db.sql(
        """ select * from `tabComposite Key`; """,
        as_dict=1,  # noqa: E501
    )

    for record in composite_keys:
        if record.get("validate_in_memory", 0) == 1:
            frappe.db.set_value(
                "Composite Key", record.name, "type", "In Memory"
            )  # noqa: E501
        else:
            frappe.db.set_value(
                "Composite Key", record.name, "type", "In Doctype Field"
            )

        frappe.db.commit()
