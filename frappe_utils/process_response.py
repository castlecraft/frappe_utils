import json

import frappe

from frappe_utils.exceptions import BadGatewayError


def process_response(response):
    reason = response.reason
    if not response.ok:
        frappe.throw(
            "Prvodier request failed, please check API"
            " or respective configuration: " + json.dumps(reason),
            BadGatewayError(reason),
        )  # noqa: 501 isort:skip

    try:
        if getattr(response, "json", None):
            reason = response.json()
        if response.ok:
            return response.json()
    except json.JSONDecodeError:
        frappe.throw(
            "Failed to parse JSON for provider API response",
            BadGatewayError(reason),
        )
