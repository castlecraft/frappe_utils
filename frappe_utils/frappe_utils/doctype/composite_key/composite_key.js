// Copyright (c) 2023, Prafful Suthar and contributors
// For license information, please see license.txt
/* global $, frappe */
frappe.composite_key = {
  set_fieldname_select: frm => {
    if (frm.doc.for_doctype) {
      frappe.model.with_doctype(frm.doc.for_doctype, () => {
        const fields = $.map(
          frappe.get_doc('DocType', frm.doc.for_doctype).fields,
          d => {
            if (
              frappe.model.no_value_type.includes(d.fieldtype) &&
              !frappe.model.table_fields.includes(d.fieldtype)
            ) {
              return null;
            } else {
              return d;
            }
          },
        );

        const allowedCompositeKeyFields = [
          'Data',
          'Long Text',
          'Long Text',
          'Small Text',
          'Text',
        ];
        const compositeKeyFields = fields
          .filter(df => allowedCompositeKeyFields.includes(df.fieldtype))
          .map(d => {
            return { label: `${d.label} (${d.fieldtype})`, value: d.fieldname };
          });

        const compositeFields = fields.map(d => {
          return { label: `${d.label} (${d.fieldtype})`, value: d.fieldname };
        });

        // add standard meta fields for composite keys
        frappe.model.std_fields.forEach(field => {
          if (field.fieldname === 'name') {
            compositeFields.unshift({
              label: 'Name (Doc Name)',
              value: 'name',
            });
          } else {
            compositeFields.push({
              label: `${field.label} (${field.fieldtype})`,
              value: field.fieldname,
            });
          }
        });

        frm.set_df_property(
          'composite_key_field',
          'options',
          compositeKeyFields,
        );

        frm
          .get_field('composite_fields')
          .grid.update_docfield_property(
            'field_name',
            'options',
            compositeFields,
          );
      });
    }
  },
};

frappe.ui.form.on('Composite Key', {
  onload: frm => {
    frappe.composite_key.set_fieldname_select(frm);
  },

  validate(frm) {
    validateCompositeFields(frm);
  },

  for_doctype(frm) {
    frm.set_value('composite_key_field', '');
    frm.doc.composite_fields = [];
    frm.refresh_fields();
    frappe.composite_key.set_fieldname_select(frm);
  },

  type(frm) {
    if (frm.doc.type === 'In Primary ID') {
      frm.set_intro(
        'User must select the <Strong>Naming Rule</Strong> as <Strong>By script</Strong> on the DocType for child tables to work correctly.',
      );
    } else {
      frm.set_intro('');
    }
  },
});

function validateCompositeFields(frm) {
  const compositeFields = frm.doc.composite_fields;

  const compositeFieldsLength = compositeFields.length;

  if (compositeFieldsLength < 2) {
    frappe.throw(
      __('There should be <strong>more than one</strong> composite fields'),
    );
  }
}
