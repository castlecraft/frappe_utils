# Copyright (c) 2023, Prafful Suthar and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document


class UserPermissionRule(Document):
    def before_insert(self):
        create_user_permissions(self)

    def after_delete(self):
        delete_linked_user_permissions(self)

    def before_save(self):
        if self.is_new():
            return

        purge_links(self)
        delete_linked_user_permissions(self)
        self.links = []
        create_user_permissions(self)
        save_links(self)


def save_links(self):
    for doc in self.links:
        doc.save()


def purge_links(self):
    for doc in self.links:
        frappe.delete_doc(doctype=doc.get("doctype"), name=doc.get("name"))


def create_user_permissions(self):
    for rule in self.permission_rules:
        perm_rule = frappe.get_cached_doc(
            "Permission Rule", rule.permission_rule
        )  # noqa: 501
        for perm_rule_links in perm_rule.permission_rule_links:
            user_perm = get_user_permission_values(self, perm_rule_links)

            user_perm_exists = frappe.db.exists(user_perm)

            if user_perm_exists and not is_user_perm_added_in_links(
                self.links, user_perm_exists
            ):
                user_perm_doc_exists = delete_user_permission(user_perm_exists)
            else:
                user_perm_doc_exists = user_perm_exists

            user_perm_doc = (
                frappe.get_cached_doc("User Permission", user_perm_doc_exists)
                if user_perm_doc_exists
                else frappe.get_doc(
                    {**user_perm, "custom_has_permission_rule": True}
                ).insert(ignore_permissions=True)
            )

            # skip duplicate links
            if not is_user_perm_added_in_links(self.links, user_perm_doc.name):
                self.append(
                    "links",
                    {
                        "link_doctype": "User Permission",
                        "link_name": user_perm_doc.name,
                    },
                )


def is_user_perm_added_in_links(links, user_perm_name):
    return any(link.link_name == user_perm_name for link in links if links)


def get_user_permission_values(self, permission_rule):
    return {
        "doctype": "User Permission",
        "user": self.user,
        "allow": permission_rule.link_doctype,
        "for_value": permission_rule.link_name,
        "apply_to_all_doctypes": False,
        "applicable_for": self.for_doctype,
    }


def delete_linked_user_permissions(self):
    for perm in self.links:
        delete_user_permission(perm.link_name)


def delete_user_permission(user_permission_name):
    """
    Delete User Permission
    If error occurs then return it's name
    """
    try:
        frappe.delete_doc("User Permission", user_permission_name)
        return None

    except frappe.LinkExistsError:
        # hide error popup from frontend
        # removes Link error from message log
        frappe.clear_last_message()

        return user_permission_name
