// Copyright (c) 2023, Prafful Suthar and contributors
// For license information, please see license.txt

/* global frappe */
frappe.ui.form.on('User Permission Rule', {
  setup(frm) {
    setPermissionRuleFilters(frm);
  },

  validate(frm) {
    checkForDuplicates(frm);
  },

  for_doctype(frm) {
    frm.clear_table('permission_rules');
    frm.add_child('permission_rules');
    setPermissionRuleFilters(frm);
    frm.refresh_fields();
  },
});

function setPermissionRuleFilters(frm) {
  frm.fields_dict.permission_rules.grid.get_field('permission_rule').get_query =
    () => {
      return {
        filters: [['Permission Rule', 'for_doctype', '=', frm.doc.for_doctype]],
      };
    };
}

function checkForDuplicates(frm) {
  const permissionRule = frm.doc.permission_rules;

  const filteredRule = permissionRule.map(item => ({
    permission_rule: item.permission_rule,
  }));

  const seen = new Set();
  for (const item of filteredRule) {
    const value = `${item.permission_rule}`;
    if (value && seen.has(value)) {
      frappe.throw(
        __(
          '<strong>Duplicate</strong> Permission Rules are <strong>not</strong> allowed',
        ),
      );
    }
    seen.add(value);
  }
}
