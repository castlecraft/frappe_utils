// Copyright (c) 2024, Prafful Suthar and contributors
// For license information, please see license.txt

frappe.ui.form.on('Access Control', {
  setup: frm => {
    frm.set_query('for_doctype', () => {
      return {
        filters: {
          issingle: 0,
          istable: 0,
        },
      };
    });
    setPermissionRuleFilters(frm);
  },

  refresh(frm) {
    hideDescendants(frm);
    addCustomButtons(frm);
    renderRoleEditor(frm);
    setPermissionRuleFilters(frm);
    setAllowDoctypeFilters(frm);
    frm.trigger('set_applicable_for_constraint');
  },

  for_doctype: frm => {
    frm.clear_table('has_permission_rule');
    frm.add_child('has_permission_rule');
    setPermissionRuleFilters(frm);
    frm.clear_table('user_permission');
    setAllowDoctypeFilters(frm);
    frm.refresh_fields();
  },

  enable_user_permission: frm => {
    if (!frm.doc.enable_user_permission) {
      frm.set_value('for_doctype', '');
    }
  },
});

frappe.ui.form.on('Access Control User Permission', {
  apply_to_all_doctypes(frm, cdt, cdn) {
    const child = locals[cdt][cdn];

    child.applicable_for = child.apply_to_all_doctypes
      ? ''
      : frm.doc.for_doctype;
    frm.refresh_field('user_permission');
  },

  allow(frm, cdt, cdn) {
    const child = locals[cdt][cdn];

    if (child.allow) {
      if (child.for_value) {
        frappe.model.set_value(child.doctype, child.name, 'for_value', null);
      }
      frm.trigger('toggle_hide_descendants', cdt, cdn);
    }
  },

  toggle_hide_descendants(frm, cdt, cdn) {
    const child = locals[cdt][cdn];
    const show = frappe.boot.nested_set_doctypes.includes(child.allow);

    frm.set_df_property(
      'user_permission',
      'hidden',
      !show,
      frm.doc.name,
      'hide_descendants',
      child.name,
    );
  },
});

function setAllowDoctypeFilters(frm) {
  if (frm.doc.for_doctype) {
    frappe.model.with_doctype(frm.doc.for_doctype, function () {
      const meta = frappe.get_meta(frm.doc.for_doctype);
      const linkFields = meta.fields
        .filter(field => field.fieldtype === 'Link')
        .map(field => field.options);

      linkFields.push(frm.doc.for_doctype);

      frm.fields_dict.user_permission.grid.get_field('allow').get_query =
        () => {
          return {
            filters: {
              name: ['in', linkFields],
            },
          };
        };
    });
  } else {
    frm.fields_dict.user_permission.grid.get_field('allow').get_query = () => {
      return {
        filters: {
          name: ['in', []],
        },
      };
    };
  }

  frm.refresh_field('user_permission');
}

function renderRoleEditor(frm) {
  if (frm.roles_editor) {
    frm.roles_editor.wrapper.empty();
  }

  const roleArea = $('<div class="role-editor">').appendTo(
    frm.fields_dict.roles_html.wrapper,
  );

  frm.roles_editor = new frappe.RoleEditor(roleArea, frm, false);

  frm.roles_editor.show();
}

function addCustomButtons(frm) {
  frm.add_custom_button('Assign to User', () => {
    frappe
      .new_doc('User Access Control', {
        module: frm.doc.module,
      })
      .then(() => {
        cur_frm.trigger('for_doctype').then(() => {
          cur_frm.clear_table('access_controls');
          cur_frm.add_child('access_controls', {
            access_control: frm.doc.name,
          });
          cur_frm.refresh_fields();
        });
      });
  });

  frm.add_custom_button('View Assigned Access Controls', () => {
    frappe.set_route('list', 'User Access Control', {
      module: frm.doc.module,
      'User Access Control Link.access_control': frm.doc.name,
    });
  });
}

function setPermissionRuleFilters(frm) {
  frm.fields_dict.has_permission_rule.grid.get_field(
    'permission_rule',
  ).get_query = () => {
    return {
      filters: [['Permission Rule', 'for_doctype', '=', frm.doc.for_doctype]],
    };
  };
}

function hideDescendants(frm) {
  frm.doc.user_permission?.forEach(x => {
    const show = frappe.boot.nested_set_doctypes.includes(x.allow);
    frm.set_df_property(
      'user_permission',
      'hidden',
      !show,
      frm.doc.name,
      'hide_descendants',
      x.name,
    );
  });
}
