// Copyright (c) 2024, Prafful Suthar and contributors
// For license information, please see license.txt

frappe.ui.form.on('User Access Control', {
  setup(frm) {
    setPermissionRuleFilters(frm);
  },

  validate(frm) {
    checkForDuplicates(frm);
  },

  module(frm) {
    frm.clear_table('access_controls');
    setPermissionRuleFilters(frm);
    frm.refresh_fields();
  },
});

function setPermissionRuleFilters(frm) {
  if (frm.doc.module) {
    frm.fields_dict.access_controls.grid.get_field('access_control').get_query =
      () => {
        return {
          filters: [['Access Control', 'module', '=', frm.doc.module]],
        };
      };
  } else {
    frm.fields_dict.access_controls.grid.get_field('access_control').get_query =
      () => {
        return {
          filters: [],
        };
      };
  }
}

function checkForDuplicates(frm) {
  const permissionRule = frm.doc.access_controls;

  const filteredRule = permissionRule.map(item => ({
    access_control: item.access_control,
  }));

  const seen = new Set();
  for (const item of filteredRule) {
    const value = `${item.access_control}`;
    if (value && seen.has(value)) {
      frappe.throw(
        __(
          '<strong>Duplicate</strong> Permission Rules are <strong>not</strong> allowed',
        ),
      );
    }
    seen.add(value);
  }
}
