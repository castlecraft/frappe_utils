# Copyright (c) 2024, Prafful Suthar and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
from frappe.utils import cstr


class UserAccessControl(Document):
    def before_insert(self):
        if frappe.db.exists(
            "User Access Control",
            {
                "user": self.get("user"),
                "module": self.get("module"),
                "docstatus": 0,
            },
        ):
            frappe.throw(
                f"Draft of '{self.get('user')}' user already exist"
                ", Please submit the existing one or use the same "
                "one to edit."
            )

    def on_submit(self):
        if self.access_controls:
            for user in self.access_controls:
                access_control_doc = frappe.get_cached_doc(
                    "Access Control", user.access_control
                )

                if self.role_action == "Assignment":
                    self.update_user_roles(access_control_doc)
                    self.create_user_permission_rule(access_control_doc)
                    self.create_user_permission(access_control_doc)
                    self.create_user_unified_application_access(
                        access_control_doc
                    )  # noqa: E501

                elif self.role_action == "Removal":
                    self.remove_user_roles(access_control_doc)
                    self.remove_user_permission_rule(access_control_doc)
                    self.remove_user_permission(access_control_doc)
                    self.remove_user_unified_application_access(
                        access_control_doc
                    )  # noqa: E501

    def update_user_roles(self, access_control_doc):
        user_doc = frappe.get_cached_doc("User", self.user)
        existing_roles = {user_role.role for user_role in user_doc.roles}  # noqa: E501

        for role in access_control_doc.roles:
            if role.role not in existing_roles:
                user_doc.append("roles", {"role": role.role})

        user_doc.save(ignore_permissions=True)

    def create_user_permission_rule(self, access_control_doc):
        if access_control_doc.has_permission_rule:
            user_perm_rule_doc = frappe.new_doc("User Permission Rule")  # noqa: E501

            user_perm_rule_doc.user = self.user
            user_perm_rule_doc.for_doctype = (
                access_control_doc.for_doctype
            )  # noqa: E501

            for perm in access_control_doc.has_permission_rule:
                user_perm_rule_doc.append(
                    "permission_rules",
                    {"permission_rule": perm.permission_rule},  # noqa: E501
                )

            user_perm_rule_doc.insert()

    def create_user_permission(self, access_control_doc):
        if not access_control_doc.enable_user_permission:
            return

        for user_perm in access_control_doc.user_permission:
            duplicate_exists = frappe.get_all(
                "User Permission",
                filters={
                    "allow": user_perm.allow,
                    "user": self.user,
                    "applicable_for": cstr(user_perm.applicable_for),
                    "apply_to_all_doctypes": user_perm.apply_to_all_doctypes,  # noqa: E501
                    "for_value": user_perm.for_value,
                },
                limit=1,
                pluck="name",
            )

            if duplicate_exists:
                duplicate_exists = duplicate_exists[0]
                user_permission_link = f'<a href="/app/Form/User Permission/{duplicate_exists}">{duplicate_exists}</a>'  # noqa: E501
                access_control_link = f'<a href="/app/Form/Access Control/{access_control_doc.name}">{access_control_doc.name}</a>'  # noqa: E501

                frappe.throw(
                    (
                        "User Permission {} already exists. Please uncheck <b>Enable User Permission</b> or select a different value for the User Permission in {}"  # noqa: E501
                    ).format(user_permission_link, access_control_link),
                    frappe.DuplicateEntryError,
                )

            user_perm_doc = frappe.new_doc("User Permission")
            user_perm_doc.user = self.user
            user_perm_doc.allow = user_perm.allow
            user_perm_doc.for_value = user_perm.for_value
            user_perm_doc.is_default = user_perm.is_default
            user_perm_doc.apply_to_all_doctypes = (
                user_perm.apply_to_all_doctypes
            )  # noqa: E501

            if user_perm.applicable_for:
                user_perm_doc.applicable_for = user_perm.applicable_for

            if user_perm.hide_descendants:
                user_perm_doc.hide_descendants = user_perm.hide_descendants

            user_perm_doc.insert()

    def create_user_unified_application_access(self, access_control_doc):
        if not access_control_doc.unified_application_name:
            return

        unified_application_access = frappe.get_cached_doc(
            "Unified Application Access",
            access_control_doc.unified_application_name,
        )

        user_unified_application_access = frappe.db.exists(
            "User Unified Application Access", self.user
        )

        if not user_unified_application_access:
            user_unified_application_access = frappe.new_doc(
                "User Unified Application Access"
            )
            user_unified_application_access.user = self.user

        else:
            user_unified_application_access = frappe.get_cached_doc(
                "User Unified Application Access", self.user
            )

        if not any(
            uaa.unified_application_access
            == access_control_doc.unified_application_name
            for uaa in user_unified_application_access.unified_applications_access  # noqa: E501
        ):
            user_unified_application_access.append(
                "unified_applications_access",
                {
                    "unified_application_access": access_control_doc.unified_application_name,  # noqa: E501
                    "description": unified_application_access.description,
                    "url": unified_application_access.url,
                    "icon": unified_application_access.icon,
                },
            )

        if user_unified_application_access.is_new():
            user_unified_application_access.insert()
        else:
            user_unified_application_access.save()

    def remove_user_roles(self, access_control_doc):
        user_doc = frappe.get_cached_doc("User", self.user)
        roles_to_remove = {role.role for role in access_control_doc.roles}  # noqa: E501

        user_doc.roles = [
            user_role
            for user_role in user_doc.roles
            if user_role.role not in roles_to_remove
        ]

        user_doc.save(ignore_permissions=True)

    def remove_user_permission_rule(self, access_control_doc):
        if access_control_doc.has_permission_rule:
            user_perm_rule_docs = frappe.get_all(
                "User Permission Rule",
                filters={
                    "user": self.user,
                    "for_doctype": access_control_doc.for_doctype,
                },  # noqa: E501
                pluck="name",
            )
            for docname in user_perm_rule_docs:
                frappe.delete_doc("User Permission Rule", docname)  # noqa: E501

    def remove_user_permission(self, access_control_doc):
        if access_control_doc.enable_user_permission:
            for user_perm in access_control_doc.user_permission:  # noqa: E501
                user_perm_docs = frappe.get_all(
                    "User Permission",
                    filters={
                        "allow": user_perm.allow,
                        "user": self.user,
                        "applicable_for": cstr(user_perm.applicable_for),
                        "apply_to_all_doctypes": user_perm.apply_to_all_doctypes,  # noqa: E501
                        "for_value": user_perm.for_value,
                    },
                    pluck="name",
                )
                for docname in user_perm_docs:
                    frappe.delete_doc("User Permission", docname)

    def remove_user_unified_application_access(self, access_control_doc):
        if not access_control_doc.unified_application_name:
            return

        user_unified_application_access = frappe.get_cached_doc(
            "User Unified Application Access", self.user
        )

        user_unified_application_access.unified_applications_access = [
            uaa
            for uaa in user_unified_application_access.unified_applications_access  # noqa: E501
            if uaa.unified_application_access
            != access_control_doc.unified_application_name  # noqa: E501
        ]

        user_unified_application_access.save(ignore_permissions=True)
