// Copyright (c) 2023, Prafful Suthar and contributors
// For license information, please see license.txt
/* global frappe, __ */

frappe.ui.form.on('Frappe Util Settings', {
  refresh(frm) {
    frm.get_field('master_meta_table').grid.only_sortable();
    frm.refresh_field('master_meta_table');

    frm.add_custom_button(__('Fetch Master Meta Doctype'), function () {
      frappe.call({
        method: 'frappe_utils.common.util.get_master_meta_doctype',
        freeze: true,
        callback(res) {
          let callSave = false;
          if (res.message) {
            const metaDoctype = [];
            for (const metaDoc of frm.doc.master_meta_table) {
              metaDoctype.push(metaDoc.meta_doctype);
            }
            for (const meta of res.message) {
              if (!metaDoctype.includes(meta)) {
                callSave = true;
                frm.add_child('master_meta_table', {
                  meta_doctype: meta,
                });
              }
            }
            frm.refresh_field('master_meta_table');
            if (callSave) frm.save();
          }
        },
      });
    });

    frm.add_custom_button(__('Sync Masters'), function () {
      const selectedMasterMeta =
        frm.fields_dict.master_meta_table.grid.get_selected_children();

      const metaDoctypeList = [];
      for (const masterMeta of selectedMasterMeta) {
        metaDoctypeList.push(masterMeta.meta_doctype);
      }

      if (metaDoctypeList.length > 0) {
        frappe.call({
          method: 'frappe_utils.api.sync_masters_api',
          args: {
            master_doctype: metaDoctypeList,
          },
          freeze: true,
          callback: res => {
            frappe.msgprint(
              'Sync master for <strong>' +
                metaDoctypeList +
                '</strong> have been enqueued as a background process, it will be synchronized in background please check error log for failures',
            );
          },
        });
      } else {
        frappe.msgprint('DocType not selected');
      }
    });
  },
});

frappe.ui.form.on('Master Meta Field', {
  force_sync_master: (frm, cdt, cdn) => {
    const row = locals[cdt][cdn];
    frappe.dom.freeze();
    frappe.call({
      method: 'frappe_utils.api.sync_masters_api',
      args: {
        force: true,
        master_doctype: [row.meta_doctype],
      },
      freeze: true,
      callback: res => {
        frappe.dom.unfreeze();
        frappe.msgprint(
          'Sync master for <strong>' +
            row.meta_doctype +
            '</strong> have been enqueued as a background process, it will be synchronized in background please check error log for failures',
        );
      },
      error: res => {
        frappe.dom.unfreeze();
      },
    });
  },
});
