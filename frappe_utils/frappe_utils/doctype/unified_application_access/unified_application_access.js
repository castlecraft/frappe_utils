// Copyright (c) 2024, Prafful Suthar and contributors
// For license information, please see license.txt

frappe.ui.form.on('Unified Application Access', {
  refresh(frm) {
    addCustomButtons(frm);
  },
});

function addCustomButtons(frm) {
  frm.add_custom_button('Assign to User', () => {
    frappe.new_doc('User Unified Application Access').then(() => {
      cur_frm.clear_table('unified_applications_access');
      cur_frm.add_child('unified_applications_access', {
        unified_application_access: frm.doc.name,
        description: frm.doc.description,
        url: frm.doc.url,
        icon: frm.doc.icon,
      });
      cur_frm.refresh_fields();
    });
  });

  frm.add_custom_button('View Assigned Unified Application Access', () => {
    frappe.set_route('list', 'User Unified Application Access', {
      'User Unified Application Access Link.unified_application_access':
        frm.doc.name,
    });
  });
}
