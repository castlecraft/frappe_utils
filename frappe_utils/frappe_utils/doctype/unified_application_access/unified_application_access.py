# Copyright (c) 2024, Prafful Suthar and contributors
# For license information, please see license.txt

# import frappe
from frappe.model.document import Document


class UnifiedApplicationAccess(Document):
    pass
