// Copyright (c) 2023, Prafful Suthar and contributors
// For license information, please see license.txt
/* global frappe, $ */
frappe.custom_provider = {
  set_fieldname_select: frm => {
    frappe.model.with_doctype('User', () => {
      const fields = $.map(frappe.get_doc('DocType', 'User').fields, d => {
        if (
          frappe.model.no_value_type.includes(d.fieldtype) &&
          !frappe.model.table_fields.includes(d.fieldtype)
        ) {
          return null;
        } else {
          return d;
        }
      });

      const allowedUserFields = ['Data'];

      const userFields = fields
        .filter(df => allowedUserFields.includes(df.fieldtype))
        .map(d => {
          return { label: `${d.label} (${d.fieldtype})`, value: d.fieldname };
        });

      frm
        .get_field('user_mapping')
        .grid.update_docfield_property('field_name', 'options', userFields);
    });
  },
};

frappe.ui.form.on('Custom Provider', {
  onload: frm => {
    frappe.custom_provider.set_fieldname_select(frm);
  },

  header: frm => {
    setHeaderTokenKey(frm);
  },
});

function setHeaderTokenKey(frm) {
  if (frm.doc.header === 'Custom') {
    frm.set_value('value', 'X-FRAPPE-CUSTOM-VALUE');
  }
}
