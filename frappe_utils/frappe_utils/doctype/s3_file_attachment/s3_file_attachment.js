// Copyright (c) 2018, Frappe and contributors
// For license information, please see license.txt

/* global frappe */
frappe.ui.form.on('S3 File Attachment', {
  refresh(frm) {},
  migrate_existing_files(frm) {
    frappe.msgprint('Local files getting migrated', 'S3 Migration');
    frappe.call({
      method: 'frappe_utils.controller.migrate_existing_files',
      callback(data) {
        if (data.message) {
          frappe.msgprint('Upload Successful');
          location.reload(true);
        } else {
          frappe.msgprint('Retry');
        }
      },
    });
  },
});
