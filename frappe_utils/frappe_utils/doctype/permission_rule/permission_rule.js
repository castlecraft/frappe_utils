// Copyright (c) 2023, Prafful Suthar and contributors
// For license information, please see license.txt

/* global frappe, $, cur_frm */
frappe.link_fields = {
  set_link_fields_filter: frm => {
    if (frm.doc.for_doctype) {
      frappe.model.with_doctype(frm.doc.for_doctype, () => {
        const fields = $.map(
          frappe.get_doc('DocType', frm.doc.for_doctype).fields,
          d => {
            if (d.fieldtype === 'Link') {
              return d.options;
            } else {
              return null;
            }
          },
        );

        frm.fields_dict.permission_rule_links.grid.get_field(
          'link_doctype',
        ).get_query = function (doc) {
          return { filters: [['DocType', 'name', 'in', fields]] };
        };
      });
    }
  },
};

frappe.ui.form.on('Permission Rule', {
  setup(frm) {
    frappe.link_fields.set_link_fields_filter(frm);
  },

  refresh: frm => {
    addCustomButtons(frm);
  },

  validate(frm) {
    checkForDuplicates(frm);
  },

  for_doctype(frm) {
    frm.doc.permission_rule_links = [];
    frappe.link_fields.set_link_fields_filter(frm);
    frm.refresh_fields();
  },

  granular_permission_check(frm) {
    setDefaultPermissions(frm);
  },
});

function addCustomButtons(frm) {
  frm.add_custom_button('Assign to User', () => {
    frappe
      .new_doc('User Permission Rule', {
        for_doctype: frm.doc.for_doctype,
      })
      .then(() => {
        cur_frm.trigger('for_doctype').then(() => {
          cur_frm.clear_table('permission_rules');
          cur_frm.add_child('permission_rules', {
            permission_rule: frm.doc.name,
          });
          cur_frm.refresh_fields();
        });
      });
  });

  frm.add_custom_button('View Assigned Permissions', () => {
    frappe.set_route('list', 'User Permission Rule', {
      for_doctype: frm.doc.for_doctype,
      'User Permission Rule Link.permission_rule': frm.doc.name,
    });
  });
}

function setDefaultPermissions(frm) {
  if (!frm.doc.granular_permission_check) {
    frm.set_value('granular_permission_select', 0);
    frm.set_value('granular_permission_read', 0);
    frm.set_value('granular_permission_write', 0);
    frm.set_value('granular_permission_create', 0);
    frm.set_value('granular_permission_delete', 0);
    frm.set_value('granular_permission_submit', 0);
    frm.set_value('granular_permission_cancel', 0);
    frm.set_value('granular_permission_amend', 0);
  } else {
    frm.set_value('granular_permission_select', 1);
    frm.set_value('granular_permission_read', 1);
  }
}

function checkForDuplicates(frm) {
  const permissionRuleLinks = frm.doc.permission_rule_links;

  const filteredLinks = permissionRuleLinks.map(item => ({
    link_doctype: item.link_doctype,
    link_name: item.link_name,
  }));

  const seen = new Set();
  for (const item of filteredLinks) {
    const value = `${item.link_doctype}-${item.link_name}`;
    if (value && seen.has(value)) {
      frappe.throw(
        __(
          '<strong>Duplicate</strong> Permission Rule Links are <strong>not</strong> allowed',
        ),
      );
    }
    seen.add(value);
  }
}
