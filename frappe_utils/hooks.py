from . import __version__

app_name = "frappe_utils"
app_title = "Frappe Utils"
app_publisher = "Prafful Suthar"
app_description = "utility tools and more around frappe framework"
app_email = "prafful@castlecraft.in"
app_license = "MIT"
app_version = __version__

doc_events = {
    "*": {
        "autoname": ["frappe_utils.doc_events_hooks.autoname.autoname"],
        "before_save": "frappe_utils.doc_events_hooks.before_save.before_save",
    },
    "User Permission": {
        "before_validate": "frappe_utils.doc_events_hooks.user_permission.before_validate",  # noqa: 501
    },
    "File": {
        "after_insert": "frappe_utils.controller.file_upload_to_s3",
        "on_trash": "frappe_utils.controller.delete_from_cloud",
    },
}

doctype_js = {
    "User Permission": "public/js/user_permission.js",
}
doctype_list_js = {"RQ Job": "public/js/rq_job_list.js"}

scheduler_events = {
    "cron": {
        "0 */6 * * *": ["frappe_utils.jobs.oauth_bearer.clear_expiry_token"],
        "0 */3 * * *": [
            "frappe_utils.jobs.redis_session_cache.clear_expired_sessions_as_batch",  # noqa: 501
        ],
    }
}

fixtures = [
    {
        "dt": "Custom Field",
        "filters": [
            [
                "name",
                "in",
                [
                    "User Permission-custom_has_permission_rule",
                ],
            ]
        ],
    }
]

permission_query_conditions = {
    "*": "frappe_utils.granular_permission.granular_permission_validator.get_permission_query_conditions",  # noqa: 501
}

has_permission = {
    "*": "frappe_utils.granular_permission.granular_permission_validator.has_permission",  # noqa: 501
}
