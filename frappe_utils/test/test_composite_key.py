import unittest

import frappe

# List to store the records and doctypes to be deleted
# Sequence is IMPORTANT
tear_down_data = {
    "User Permission": [],
    "Composite Key": [],
    "Custom Customer": [],
    "User": [],
    "DocType": [],
}


def set_up_doctype():
    # Customer doctype
    customer_doctype = frappe.get_doc(
        {
            "doctype": "DocType",
            "module": "Frappe Utils",
            "naming_rule": "Expression",
            "autoname": "format:{first_name}",
            "fields": [
                {
                    "fieldname": "first_name",
                    "label": "First Name",
                    "fieldtype": "Data",
                    "reqd": 1,
                },
                {
                    "fieldname": "last_name",
                    "label": "Last Name",
                    "fieldtype": "Data",
                },
                {
                    "fieldname": "age",
                    "label": "Age",
                    "fieldtype": "Int",
                },
                {
                    "default": "Male",
                    "fieldname": "gender",
                    "label": "Gender",
                    "fieldtype": "Select",
                    "options": "Male\nFemale\nOther",
                },
                {
                    "fieldname": "composite_key",
                    "fieldtype": "Data",
                    "label": "Composite Key",
                    "read_only": 1,
                },
            ],
            "permissions": [
                {
                    "role": "Sales Manager",
                    "read": 1,
                    "write": 0,
                    "create": 0,
                    "delete": 0,
                },
                {
                    "role": "System Manager",
                    "read": 1,
                    "write": 1,
                    "create": 1,
                    "delete": 1,
                },
            ],
        }
    )
    customer_doctype.name = "Custom Customer"
    customer_doctype.insert()
    tear_down_data["DocType"].append(customer_doctype.name)


def create_entries_in_custom_customer():
    new_doc = frappe.get_doc(
        {
            "doctype": "Custom Customer",
            "first_name": "John",
            "last_name": "Doe",
            "age": 25,
            "gender": "Male",
        }
    )
    new_doc.insert()
    tear_down_data["Custom Customer"].append(new_doc.name)


def create_composite_key(doctype, key_seperator, key_field, composite_fields):
    new_doc = frappe.get_doc(
        {
            "doctype": "Composite Key",
            "for_doctype": doctype,
            "key_separator": key_seperator,
            "composite_key_field": key_field,
        }
    )
    new_doc.insert()
    frappe.db.commit()
    tear_down_data["Composite Key"].append(new_doc.name)

    # For composite Field
    for index, field in enumerate(composite_fields):
        new_doc = frappe.get_doc(
            {
                "doctype": "Composite Key Field",
                "field_name": field,
                "idx": index,
                "parent": doctype,
                "parentfield": "Composite_fields",
                "parenttype": "Composite Key",
            }
        )
        new_doc.insert()
        frappe.db.commit()


def create_user(name, email):
    user_doc = frappe.get_doc(
        {
            "doctype": "User",
            "first_name": name,
            "email": email,
            "roles": [{"role": "System Manager"}],
        }
    )
    user_doc.insert()
    if user_doc.first_name != "Anonymous":
        # Do not delete the Dummy User
        tear_down_data["User"].append(user_doc.email)


class TestCompositeKey(unittest.TestCase):
    frappe.set_user("Administrator")
    exists = frappe.db.exists("User", "anonymous@gmail.com")
    if not exists:
        # Create a Dummy User with System Manager rights
        # if not present already and forget it
        create_user("Anonymous", "anonymous@gmail.com")

    def setUp(self):
        set_up_doctype()
        create_composite_key(
            "Custom Customer",
            "-",
            "composite_key",
            ["last_name", "first_name", "age", "gender"],
        )
        create_entries_in_custom_customer()

    def tearDown(self):
        # DB Cleaning goes here....
        frappe.set_user("Administrator")

        # Remove all other doctypes
        for key, array in tear_down_data.items():
            for element in array:
                frappe.delete_doc(key, element)

    def test_composite_key_on_save(self):
        customer_doc = frappe.get_doc("Custom Customer", "John")
        composite_key_doc = frappe.get_doc(
            "Composite Key", customer_doc.doctype  # noqa: E501
        )
        composite_key_value = customer_doc.composite_key

        # Hard coding composite key value for testing
        composite_key_fields = []
        key_seperator = composite_key_doc.key_separator

        for row in composite_key_doc.composite_fields:
            composite_key_fields.append(
                str(customer_doc.get(row.field_name))
            )  # noqa: E501

        value = key_seperator.join(composite_key_fields)

        # Comparing composite key values
        self.assertEqual(
            composite_key_value, value, f"composite key should be '{value}'"
        )
