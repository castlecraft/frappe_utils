import unittest

import frappe

from frappe_utils.frappe_perm_parser.frappe_perm_parser import has_valid_perms

# List to store the records and doctypes to be deleted
# Sequence is IMPORTANT
tear_down_data = {
    "User Permission": [],
    "Custom Invoice": [],
    "Custom Customer": [],
    "Custom Warehouse": [],
    "User": [],
}

number_of_doc = 2


def set_up_doctype():
    # Customer doctype
    customer_doctype = frappe.get_doc(
        {
            "doctype": "DocType",
            "module": "Frappe Utils",
            "naming_rule": "Expression",
            "autoname": "format:{name1}",
            "fields": [
                {
                    "fieldname": "name1",
                    "label": "Name",
                    "fieldtype": "Data",
                    "reqd": 1,
                },
            ],
            "permissions": [
                {
                    "role": "Sales Manager",
                    "read": 1,
                    "write": 0,
                    "create": 0,
                    "delete": 0,
                },
                {
                    "role": "System Manager",
                    "read": 1,
                    "write": 1,
                    "create": 1,
                    "delete": 1,
                },
            ],
        }
    )
    customer_doctype.name = "Custom Customer"
    customer_doctype.insert()

    # Warehouse doctype
    warehouse_doctype = frappe.get_doc(
        {
            "doctype": "DocType",
            "module": "Frappe Utils",
            "naming_rule": "Expression",
            "autoname": "format:{name1}",
            "fields": [
                {
                    "fieldname": "name1",
                    "label": "Name",
                    "fieldtype": "Data",
                    "reqd": 1,
                },
            ],
            "permissions": [
                {
                    "role": "Sales Manager",
                    "read": 1,
                    "write": 0,
                    "create": 0,
                    "delete": 0,
                },
                {
                    "role": "System Manager",
                    "read": 1,
                    "write": 1,
                    "create": 1,
                    "delete": 1,
                },
            ],
        }
    )
    warehouse_doctype.name = "Custom Warehouse"
    warehouse_doctype.insert()

    # Invoice doctype
    invoice_doctype = frappe.get_doc(
        {
            "doctype": "DocType",
            "module": "Frappe Utils",
            "naming_rule": "Expression",
            "autoname": "format:{name1}",
            "fields": [
                {
                    "fieldname": "name1",
                    "label": "Name",
                    "fieldtype": "Data",
                    "reqd": 1,
                },
                {
                    "fieldname": "customer",
                    "label": "Customer",
                    "fieldtype": "Link",
                    "options": "Custom Customer",
                },
                {
                    "fieldname": "warehouse",
                    "label": "Warehouse",
                    "fieldtype": "Link",
                    "options": "Custom Warehouse",
                },
            ],
            "permissions": [
                {
                    "role": "Sales Manager",
                    "read": 1,
                    "write": 0,
                    "create": 0,
                    "delete": 0,
                },
                {
                    "role": "System Manager",
                    "read": 1,
                    "write": 1,
                    "create": 1,
                    "delete": 1,
                },
            ],
        }
    )
    invoice_doctype.name = "Custom Invoice"
    invoice_doctype.insert()


def create_entries_in_custom_doctypes(custom_doctype):
    for i in range(number_of_doc):
        new_doc = frappe.get_doc(
            {
                "doctype": f"Custom {custom_doctype}",
                "name1": f"{custom_doctype} {i+1}",
            }
        )
        new_doc.insert()
        tear_down_data[("Custom " + custom_doctype)].append(new_doc.name)


def create_entries_in_custom_invoice():
    for count in range(
        1, (number_of_doc * number_of_doc * number_of_doc) // 2 + 1
    ):  # noqa
        j = ((count - 1) // number_of_doc) % number_of_doc
        k = (count - 1) % number_of_doc

        invoice_doc = frappe.get_doc(
            {
                "doctype": "Custom Invoice",
                "name1": f"invoice {count}",
                "customer": f"customer {j+1}",
                "warehouse": f"warehouse {k+1}",
            }
        )
        invoice_doc.insert()
        tear_down_data["Custom Invoice"].append(invoice_doc.name)


def create_user(name, email):
    user_doc = frappe.get_doc(
        {
            "doctype": "User",
            "first_name": name,
            "email": email,
            "roles": [{"role": "System Manager"}],
        }
    )
    user_doc.insert()
    if user_doc.first_name != "Anonymous":
        # Do not delete the Dummy User
        tear_down_data["User"].append(user_doc.email)


def delete_entry(doctype, value):
    frappe.get_doc(doctype, value).delete()
    if doctype == "Doctype":
        tear_down_data[doctype].clear
    else:
        x = value["user"] if value["user"] else value
        tear_down_data[doctype].remove(x)


def create_user_permission(doctype, value, applicable_for=None):
    frappe.set_user("Administrator")
    user = frappe.get_doc("User", {"email": tear_down_data["User"][0]})

    user_permission_doc = frappe.get_doc(
        {
            "doctype": "User Permission",
            "user": user.name,
            "allow": doctype,
            "for_value": value,
            "applicable_for": applicable_for,
        }
    )
    user_permission_doc.insert()
    frappe.db.commit()
    tear_down_data["User Permission"].append(user_permission_doc.as_dict())

    # Filtering tear_down_data["User Permission"].
    tear_down_data["User Permission"] = [
        {
            "user": permission["user"],
            "for_value": permission["for_value"],
            "allow": permission["allow"],
            "applicable_for": permission["applicable_for"],
        }
        for permission in tear_down_data["User Permission"]
    ]


class TestQueryBuilderPerms(unittest.TestCase):
    frappe.set_user("Administrator")
    exists = frappe.db.exists("User", "anonymous@gmail.com")
    if not exists:
        # Create a Dummy User with System Manager rights
        # if not present already and forget it
        create_user("Anonymous", "anonymous@gmail.com")

    def setUp(self):
        set_up_doctype()
        create_entries_in_custom_doctypes("Customer")
        create_entries_in_custom_doctypes("Warehouse")
        create_entries_in_custom_invoice()
        create_user("New User", "newuser@gmail.com")

    def tearDown(self):
        # DB Cleaning goes here....
        frappe.set_user("Administrator")
        # Remove User Permission
        for permission in tear_down_data["User Permission"]:
            user_permission = frappe.get_doc(
                "User Permission",
                {
                    "user": permission["user"],
                    "allow": permission["allow"],
                    "for_value": permission["for_value"],
                },
            )
            user_permission.delete()
        tear_down_data["User Permission"].clear()

        # Remove all other doctypes
        for key, array in tear_down_data.items():
            for element in array:
                frappe.delete_doc(key, element)
                # This condition is very important otherwise
                # Frappe doctypes will be deleted and will have to reinstall
            if key != "User Permission" and key != "User":
                frappe.delete_doc("DocType", key)
                tear_down_data[key].clear()

    def test_multi_doc_permissions(self):
        # Setting up user permission.
        create_user_permission(
            "Custom Customer", "Customer 1", "Custom Customer"
        )  # Self doctype type permission
        create_user_permission(
            "Custom Customer", "Customer 2"
        )  # All doctype permission
        create_user_permission(
            "Custom Warehouse", "warehouse 2", "Custom Invoice"
        )  # specific doctype permission
        create_user_permission(
            "Custom Invoice", "invoice 4"
        )  # All doctype permission #noqa

        # Testing till no permission left in tear_down_data["User Permission"]
        while len(tear_down_data["User Permission"]) != 0:
            frappe.set_user(tear_down_data["User"][0])
            customer = frappe.qb.DocType("Custom Customer")
            warehouse = frappe.qb.DocType("Custom Warehouse")
            invoice = frappe.qb.DocType("Custom Invoice")

            query_for_customer = frappe.qb.from_(customer).select("name")
            query_for_warehouse = frappe.qb.from_(warehouse).select("name")
            query_for_invoice = frappe.qb.from_(invoice).select("name")

            customer_from_get_list = frappe.get_list("Custom Customer")
            warehouse_from_get_list = frappe.get_list("Custom Warehouse")
            invoice_from_get_list = frappe.get_list("Custom Invoice")

            # Checking for customer.
            result = has_valid_perms(
                qb=query_for_customer, user=tear_down_data["User"][0]
            )
            items_from_valid_perms = result.run(as_dict=True)
            self.assertCountEqual(
                customer_from_get_list,
                items_from_valid_perms,
                f"\n\n Items received from has_valid_perms(): {items_from_valid_perms} \n Items received from get_list(): {customer_from_get_list} \n User Permission: {tear_down_data['User Permission']}",  # noqa
            )

            # Checking for Warehouse.
            result = has_valid_perms(
                qb=query_for_warehouse, user=tear_down_data["User"][0]
            )
            items_from_valid_perms = result.run(as_dict=True)
            self.assertCountEqual(
                warehouse_from_get_list,
                items_from_valid_perms,
                f"\n Items received from has_valid_perms(): {items_from_valid_perms} \n Items received from get_list(): {warehouse_from_get_list} \n User Permission: {tear_down_data['User Permission']}",  # noqa
            )

            # Checking for Invoice.
            result = has_valid_perms(
                qb=query_for_invoice, user=tear_down_data["User"][0]
            )
            items_from_valid_perms = result.run(as_dict=True)
            self.assertCountEqual(
                invoice_from_get_list,
                items_from_valid_perms,
                f"\n Items received from has_valid_perms(): {items_from_valid_perms} \n Items received from get_list(): {invoice_from_get_list} \n User Permission: {tear_down_data['User Permission']}",  # noqa
            )

            # Remove last added user permission
            frappe.get_doc(
                "User Permission",
                {
                    "user": tear_down_data["User"][0],
                    "allow": tear_down_data["User Permission"][
                        len(tear_down_data["User Permission"]) - 1
                    ][
                        "allow"
                    ],  # remove last added permission
                    "for_value": tear_down_data["User Permission"][
                        len(tear_down_data["User Permission"]) - 1
                    ]["for_value"],
                },
            ).delete()
            tear_down_data["User Permission"].pop()

    # def test_query_builder_with_join(self):
    #     pass

    # def test_query_builder_with_groups(self):
    #     pass
