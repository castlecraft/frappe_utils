import frappe

from frappe_utils.test_helper.util_testing_helper import UtilTestingHelper

from frappe_utils.test_helper.decorator import (  # type: ignore #isort: skip
    bootstrap_doctype,
    spin_examples,
)

from frappe_utils.test.sandbox_test.customer.sandbox_customer_bootstrap import (  # noqa: E501 #isort: skip
    bootstrap_customer,
)

__doctype = "Custom Invoice"

helper = UtilTestingHelper()


def __bootstrap_dependant():
    bootstrap_customer()


@bootstrap_doctype(__doctype)
def bootstrap_invoice(create_example=True):
    __bootstrap_dependant()

    # Invoice doctype
    invoice_doctype = frappe.get_doc(
        {
            "doctype": "DocType",
            "module": "Frappe Utils",
            "naming_rule": "Expression",
            "autoname": "format:{name1}",
            "fields": [
                {
                    "fieldname": "name1",
                    "label": "Name",
                    "fieldtype": "Data",
                    "reqd": 1,
                },
                {
                    "fieldname": "customer",
                    "label": "Customer",
                    "fieldtype": "Link",
                    "options": "Custom Customer",
                },
            ],
            "permissions": [
                {
                    "role": "Sales Manager",
                    "read": 1,
                    "write": 0,
                    "create": 0,
                    "delete": 0,
                },
                {
                    "role": "System Manager",
                    "read": 1,
                    "write": 1,
                    "create": 1,
                    "delete": 1,
                },
            ],
        }
    )

    invoice_doctype.name = __doctype
    invoice_doctype.insert()

    helper.append_tear_down_doc("DocType", invoice_doctype.name)
    True if create_example is False else __spin_examples()


@spin_examples(__doctype)
def __spin_examples():
    return [
        {
            "doctype": __doctype,
            "name1": "{} {}".format(
                frappe.mock("first_name"), frappe.mock("iana_id")  # noqa: E501
            ),
            "customer": helper.get_sample_doc_names("Custom Customer")[0],
        },
        {
            "doctype": __doctype,
            "name1": "{} {}".format(
                frappe.mock("first_name"), frappe.mock("iana_id")  # noqa: E501
            ),
            "customer": helper.get_sample_doc_names("Custom Customer")[1],
        },
        {
            "doctype": __doctype,
            "name1": "{} {}".format(
                frappe.mock("first_name"), frappe.mock("iana_id")  # noqa: E501
            ),
            "customer": helper.get_sample_doc_names("Custom Customer")[2],
        },
    ]
