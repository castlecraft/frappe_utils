import frappe

from frappe_utils.test_helper.testing_base import FrappeTestingBase
from frappe_utils.test_helper.util_testing_helper import UtilTestingHelper

from frappe_utils.test.sandbox_test.customer.sandbox_customer_bootstrap import (  # noqa: E501 #isort: skip
    bootstrap_customer,
)  # noqa: E501
from frappe_utils.test.sandbox_test.invoice.sandbox_invoice_bootstrap import (  # noqa: E501 #isort: skip
    bootstrap_invoice,
)  # noqa: E501

helper = UtilTestingHelper()


class TestCompositeKey(FrappeTestingBase):
    def setUp(self):
        bootstrap_invoice()
        bootstrap_customer()

    def test_invoice_on_save(self):
        # Your Test Logic.
        invoice = frappe.get_doc(
            "Custom Invoice",
            helper.get_sample_doc_names("Custom Invoice")[0],
        )

        customer_doc = frappe.get_doc(
            "Custom Customer", invoice.get("customer")  # noqa: E501
        )

        self.assertIsNotNone(
            customer_doc,
            "Custom Customer Doc not found {}".format(invoice.get("customer")),
        )
