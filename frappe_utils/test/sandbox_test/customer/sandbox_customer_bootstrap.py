import frappe

from frappe_utils.test_helper.util_testing_helper import UtilTestingHelper

from frappe_utils.test_helper.decorator import (  # type: ignore #isort: skip
    bootstrap_doctype,
    spin_examples,
)

__doctype = "Custom Customer"

helper = UtilTestingHelper()


def __bootstrap_dependant():
    pass


@bootstrap_doctype(__doctype)
def bootstrap_customer(create_example=True):
    __bootstrap_dependant()

    # Customer doctype
    customer_doctype = frappe.get_doc(
        {
            "doctype": "DocType",
            "module": "Frappe Utils",
            "naming_rule": "Expression",
            "autoname": "format:{name1}",
            "fields": [
                {
                    "fieldname": "name1",
                    "label": "Name",
                    "fieldtype": "Data",
                    "reqd": 1,
                },
            ],
            "permissions": [
                {
                    "role": "Sales Manager",
                    "read": 1,
                    "write": 0,
                    "create": 0,
                    "delete": 0,
                },
                {
                    "role": "System Manager",
                    "read": 1,
                    "write": 1,
                    "create": 1,
                    "delete": 1,
                },
            ],
        }
    )

    customer_doctype.name = __doctype
    customer_doctype.insert()

    helper.append_tear_down_doc("DocType", customer_doctype.name)
    True if create_example is False else __spin_examples()


@spin_examples(__doctype)
def __spin_examples():
    return [
        {
            "doctype": __doctype,
            "name1": "{} {}".format(
                frappe.mock("first_name"), frappe.mock("iana_id")  # noqa: E501
            ),
        },
        {
            "doctype": __doctype,
            "name1": "{} {}".format(
                frappe.mock("first_name"), frappe.mock("iana_id")  # noqa: E501
            ),
        },
        {
            "doctype": __doctype,
            "name1": "{} {}".format(
                frappe.mock("first_name"), frappe.mock("iana_id")  # noqa: E501
            ),
        },
    ]
