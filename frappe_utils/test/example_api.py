import json
from http.server import BaseHTTPRequestHandler, HTTPServer


class ExampleServer(BaseHTTPRequestHandler):
    def do_GET(self):
        sampleData = {}
        if self.path == "/get_token/email":
            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            sampleData = {
                "data": {
                    "emailId": "anonymous.user@example.com",
                    "fullName": "Anonymous",
                },
            }

        if self.path == "/get_token/no_email":
            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            sampleData = {
                "data": {
                    "domain": "anonymous.user",
                    "fullName": "Anonymous",
                    "employee_id": "EMP-000",
                    "mobile": "9876543210",
                    "host": "example.com",
                },
            }

        if self.path == "/get_token/error":
            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            sampleData = {
                "status": "failed to decrypt",
                "message": "",
            }

        self.wfile.write(json.dumps(sampleData).encode("utf-8"))


webserver = HTTPServer(("localhost", 8080), ExampleServer)
webserver.serve_forever()
