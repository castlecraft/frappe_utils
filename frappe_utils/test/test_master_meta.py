import json
import unittest

import frappe
from dev_utils.utils.create_fixtures import create_fixtures

from frappe_utils.commands.site import execute_sync_masters

custom_notes_doctype_name = "Custom Notes"
custom_notes_doc = {
    "title": "Note Title",
    "description": "Note Description",
    "link_title": "Link Note Title",
    "link_description": "Link Note Description",
    "table_title": "Table Note Title",
    "table_description": "Table Note Description",
}

custom_status_doctype_name = "Custom Status"
custom_status_doc = "Enabled"

custom_table_doctype_name = "Custom Table"
custom_table_doc = "List Item Data"


def create_custom_notes_doctype():
    notes_doctype = frappe.get_doc(
        {
            "doctype": "DocType",
            "module": "Dev Utils",
            "naming_rule": "Expression",
            "autoname": "format:{title}",
            "fields": [
                {
                    "fieldname": "title",
                    "label": "Title",
                    "fieldtype": "Data",
                },
                {
                    "fieldname": "description",
                    "label": "Description",
                    "fieldtype": "Data",
                },
                {
                    "fieldname": "status",
                    "label": "Status",
                    "fieldtype": "Link",
                    "options": custom_status_doctype_name,
                },
                {
                    "fieldname": "custom_list_table",
                    "label": "List Table",
                    "fieldtype": "Table",
                    "options": "Custom Table",
                },
            ],
            "permissions": [
                {
                    "role": "System Manager",
                    "read": 1,
                    "write": 1,
                    "create": 1,
                    "delete": 1,
                }
            ],
        }
    )
    notes_doctype.name = custom_notes_doctype_name
    notes_doctype.insert()
    frappe.db.commit()


def create_custom_notes_doc(type=""):
    note_doc = frappe.get_doc(
        {
            "doctype": custom_notes_doctype_name,
            "title": custom_notes_doc[type + "title"],
            "description": custom_notes_doc[type + "description"],
        }
    )
    if type == "link_":
        note_doc.status = custom_status_doc
    if type == "table_":
        note_doc.append(
            "custom_list_table", {"custom_list_item": custom_table_doc}
        )  # noqa: 501
    note_doc.insert()
    frappe.db.commit()


def create_custom_status_doctype():
    status_doctype = frappe.get_doc(
        {
            "doctype": "DocType",
            "module": "Dev Utils",
            "naming_rule": "Expression",
            "autoname": "format:{custom_status_name}",
            "fields": [
                {
                    "fieldname": "custom_status_name",
                    "label": "Custom Status Name",
                    "fieldtype": "Data",
                },
            ],
            "permissions": [
                {
                    "role": "System Manager",
                    "read": 1,
                    "write": 1,
                    "create": 1,
                    "delete": 1,
                }
            ],
        }
    )
    status_doctype.name = custom_status_doctype_name
    status_doctype.insert()
    frappe.db.commit()


def create_custom_status_doc():
    status_doc = frappe.get_doc(
        {
            "doctype": custom_status_doctype_name,
            "custom_status_name": custom_status_doc,
        }
    )
    status_doc.insert()
    frappe.db.commit()


def create_custom_table_doctype():
    table_doctype = frappe.get_doc(
        {
            "doctype": "DocType",
            "module": "Dev Utils",
            "naming_rule": "Expression",
            "autoname": "format:{custom_list_item}",
            "istable": True,
            "fields": [
                {
                    "fieldname": "custom_list_item",
                    "label": "Custom List Item",
                    "fieldtype": "Data",
                    "in_list_view": True,
                },
            ],
            "permissions": [
                {
                    "role": "System Manager",
                    "read": 1,
                    "write": 1,
                    "create": 1,
                    "delete": 1,
                }
            ],
        }
    )
    table_doctype.name = custom_table_doctype_name
    table_doctype.insert()
    frappe.db.commit()


def set_master_meta():
    master_meta_doc = frappe.get_doc(
        {
            "doctype": "Master Meta",
            "meta_doctype": custom_notes_doctype_name,
            "doctype_owner": "frappe",
            "exported_app": "integration_test",
        }
    )
    master_meta_doc.insert()
    frappe.db.commit()


def get_json_data():
    file_path = (
        "/masters/"
        + custom_notes_doctype_name.lower().replace(" ", "_")
        + "/data.json"  # noqa: 501
    )
    with open(frappe.get_app_path("integration_test") + file_path) as f:
        return json.loads(f.read())[0]


def check_json_data(type=""):
    data = get_json_data()
    if (
        not data["title"] == custom_notes_doc[type + "title"]
        and not data["description"] == custom_notes_doc[type + "description"]
    ):
        frappe.throw("Masters sync failed")
    if type == "link_" and not data["status"] == custom_status_doc:
        frappe.throw("Masters sync failed")
    if (
        type == "table_"
        and not data["custom_list_table"][0]["custom_list_item"]
        == custom_table_doc  # noqa: 501
    ):
        frappe.throw("Masters sync failed")


def delete_docs():
    notes = frappe.get_list(custom_notes_doctype_name)
    for note in notes:
        frappe.delete_doc(custom_notes_doctype_name, note.name)
    frappe.delete_doc(custom_status_doctype_name, custom_status_doc)
    frappe.db.commit()


def delete_doctypes():
    frappe.delete_doc("DocType", custom_status_doctype_name)
    frappe.delete_doc("DocType", custom_notes_doctype_name)
    frappe.delete_doc("DocType", custom_table_doctype_name)
    frappe.db.commit()


def delete_master_meta():
    frappe.delete_doc("Master Meta", custom_notes_doctype_name)
    frappe.db.commit()


def delete_custom_notes_doc(type=""):
    frappe.delete_doc(
        custom_notes_doctype_name, custom_notes_doc[type + "title"]
    )  # noqa: 501
    frappe.db.commit()


def check_custom_notes_doc(type=""):
    note_doc = frappe.get_doc(
        custom_notes_doctype_name, custom_notes_doc[type + "title"]
    )
    if (
        not note_doc.title == custom_notes_doc[type + "title"]
        and not note_doc.description == custom_notes_doc[type + "description"]
    ):
        frappe.throw("Masters sync failed")
    if type == "link_" and not note_doc.status == custom_status_doc:
        frappe.throw("Masters sync failed")
    if (
        type == "table_"
        and not note_doc.custom_list_table[0].custom_list_item
        == custom_table_doc  # noqa: 501
    ):
        frappe.throw("Masters sync failed")


def revoke_settings():
    doc = frappe.get_doc("Frappe Util Settings")
    doc.master_meta_table = []
    doc.save()
    frappe.db.commit()


class TestMasterMeta(unittest.TestCase):
    frappe.set_user("Administrator")

    def setUp(self):
        create_custom_status_doctype()
        create_custom_table_doctype()
        create_custom_notes_doctype()

    def test_notes(self):
        create_custom_notes_doc()
        set_master_meta()
        create_fixtures(custom_notes_doctype_name)
        check_json_data()
        delete_custom_notes_doc()
        execute_sync_masters()
        check_custom_notes_doc()

    def test_with_link(self):
        create_custom_status_doc()
        create_custom_notes_doc("link_")
        set_master_meta()
        create_fixtures(custom_notes_doctype_name)
        check_json_data("link_")
        delete_custom_notes_doc("link_")
        execute_sync_masters()
        check_custom_notes_doc("link_")

    def test_with_table(self):
        create_custom_notes_doc("table_")
        set_master_meta()
        create_fixtures(custom_notes_doctype_name)
        check_json_data("table_")
        delete_custom_notes_doc("table_")
        execute_sync_masters()
        check_custom_notes_doc("table_")

    def tearDown(self):
        delete_master_meta()
        delete_docs()
        delete_doctypes()
        revoke_settings()
