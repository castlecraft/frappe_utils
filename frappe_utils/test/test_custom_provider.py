import unittest

import frappe
import requests

from frappe_utils.auth import get_data

api_response = {}
provider = {
    "oauth_client": "Example",
    "token": "token",
    "referer": "https://www.google.com",
    "provider_name": frappe.mock("first_name"),
    "api_endpoint": "http://localhost:8000/api/method/integration_test.custom_provider.api.get_token_no_email",  # noqa: E501
    "email_api_endpoint_url": "http://localhost:8000/api/method/integration_test.custom_provider.api.get_token_email",  # noqa: E501
    "name_map": "data.fullName",
    "email_map": "data.emailId",
    "prefix": "data.employee_id",
    "separator": "_",
    "suffix": "data.mobile",
    "domain": frappe.mock("domain_name"),
}


def fetch_endpoint_data(endpoint):
    headers = {"Token": provider["token"]}
    global api_response
    api_response = requests.get(endpoint, headers=headers).json()


def create_oauth_client():
    oauth_client_doc = frappe.get_doc(
        {
            "doctype": "OAuth Client",
            "app_name": provider["oauth_client"],
            "scopes": "openid all",
            "default_redirect_uri": provider["referer"],
        }
    )
    oauth_client_doc.insert()
    frappe.db.commit()


def create_perm_rule_doc(no_perm_rule, no_doc):
    perm_rules = []

    for doctype in no_doc:
        for i in range(no_perm_rule):
            perm_rule_doc = frappe.get_doc(
                {
                    "doctype": "Permission Rule",
                    "for_doctype": doctype,
                    "permission_rule_name": f"{doctype}_Anonymous_{i+1}",
                }
            )
            perm_rules.append(f"{doctype}_Anonymous_{i+1}")
            perm_rule_doc.insert()

    frappe.db.commit()
    return perm_rules


def remove_perm_rule_doc(perm_rule_doc):
    provider_doc = frappe.get_doc("Custom Provider", provider["provider_name"])
    provider_doc.set("permission_rules", [])
    provider_doc.save(ignore_permissions=True)

    for doc_name in perm_rule_doc:
        frappe.delete_doc("Permission Rule", doc_name)
    frappe.db.commit()


def create_custom_provider_with_rules(no_perm_rule, no_doc):
    permission_rules = create_perm_rule_doc(
        no_perm_rule=no_perm_rule, no_doc=no_doc
    )  # noqa: E501
    permission_rules_list = [
        {
            "permission_rule": rule,
            "condition": "True",
        }
        for rule in permission_rules
    ]  # noqa: E501

    provider_doc = frappe.get_doc("Custom Provider", provider["provider_name"])
    provider_doc.set("permission_rules", permission_rules_list)
    provider_doc.save(ignore_permissions=True)
    remove_perm_rule_doc(permission_rules)


def create_custom_provider(endpoint, create_session=False):
    oauth_client_doc = frappe.db.get(
        "OAuth Client", filters={"app_name": provider["oauth_client"]}
    )  # noqa: 501

    provider_doc = frappe.get_doc(
        {
            "doctype": "Custom Provider",
            "enable": True,
            "method": "GET",
            "email_map": provider["email_map"],
            "bypass_tls_validate": True,
            "header": "Token",
            "oauth_client": oauth_client_doc.client_id,
            "provider_name": provider["provider_name"],
            "referer": provider["referer"],
            "url": endpoint,
            "value": provider["token"],
            "create_user_flag": True,
            "roles": [{"role": "System Manager"}],
            "create_session": create_session,
        }
    )
    provider_doc.insert()
    frappe.db.commit()


def set_name_map():
    provider_doc = frappe.get_doc("Custom Provider", provider["provider_name"])
    provider_doc.name_map = provider["name_map"]
    provider_doc.save()
    frappe.db.commit()


def create_domain():
    domain_doc = frappe.get_doc(
        {"doctype": "Domain", "domain": provider["domain"]}
    )  # noqa: 501
    domain_doc.insert()
    frappe.db.commit()


def set_non_email_user():
    provider_doc = frappe.get_doc("Custom Provider", provider["provider_name"])
    provider_doc.non_email_user_flag = True
    provider_doc.prefix = provider["prefix"]
    provider_doc.separator = provider["separator"]
    provider_doc.suffix = provider["suffix"]
    provider_doc.domain = frappe.get_doc("Domain", provider["domain"])
    provider_doc.save()
    frappe.db.commit()


def generate_bearer_token():
    url = frappe.utils.get_url() + "/api/method/frappe_utils.auth.validate"
    headers = {
        "X-FRAPPE-CUSTOM-HEADER": provider["provider_name"],
        "Referer": provider["referer"],
        "Token": provider["token"],
    }
    return requests.get(url, headers=headers)


def check_bearer_token(user):
    access_tokens = frappe.db.get_list(
        "OAuth Bearer Token", filters={"user": user}
    )  # noqa: 501
    if not access_tokens:
        frappe.throw("OAuth Bearer Token not generated")


def verify_session_user(response):
    data = response.json()
    user = (
        (
            data.get("email")
            and isinstance(data["email"], dict)
            and "name" in data["email"]
            and data["email"]["name"]
        )
        or (isinstance(data.get("email"), str) and data["email"])
        or ""
    )

    cookies = {"sid": response.cookies.get("sid")}
    url_session = (
        frappe.utils.get_url() + "/api/method/frappe.auth.get_logged_user"
    )  # noqa: E501
    session_user = requests.get(url_session, cookies=cookies)

    if user != session_user.json().get("message"):
        frappe.throw("Invalid session user")

    from frappe.sessions import clear_sessions

    clear_sessions(user=session_user.json().get("message"), force=True)


def remove_domain():
    frappe.delete_doc("Domain", provider["domain"])
    frappe.db.commit()


def remove_bearer_tokens(user):
    access_tokens = frappe.get_list(
        "OAuth Bearer Token", filters={"user": user}
    )  # noqa: 501
    for token in access_tokens:
        frappe.delete_doc("OAuth Bearer Token", token.name)
    frappe.db.commit()


def remove_user(user):
    frappe.delete_doc("User", user)
    frappe.db.commit()


def remove_custom_provider():
    frappe.delete_doc("Custom Provider", provider["provider_name"])
    frappe.db.commit()


def remove_oauth_client():
    frappe.db.delete(
        "OAuth Client", filters={"app_name": provider["oauth_client"]}
    )  # noqa: 501
    frappe.db.commit()


class TestCustomProvider(unittest.TestCase):
    frappe.set_user("Administrator")

    def test_email_user(self):
        fetch_endpoint_data(provider["email_api_endpoint_url"])
        create_oauth_client()
        create_custom_provider(provider["email_api_endpoint_url"])
        generate_bearer_token()
        check_bearer_token(get_data(api_response, provider["email_map"]))

        remove_bearer_tokens(get_data(api_response, provider["email_map"]))
        remove_custom_provider()
        remove_oauth_client()
        remove_user(get_data(api_response, provider["email_map"]))

    def test_named_user(self):
        fetch_endpoint_data(provider["email_api_endpoint_url"])
        create_oauth_client()
        create_custom_provider(provider["email_api_endpoint_url"])
        set_name_map()
        generate_bearer_token()
        check_bearer_token(get_data(api_response, provider["email_map"]))

        remove_bearer_tokens(get_data(api_response, provider["email_map"]))
        remove_custom_provider()
        remove_oauth_client()
        remove_user(get_data(api_response, provider["email_map"]))

    def test_non_email_user(self):
        fetch_endpoint_data(provider["api_endpoint"])
        create_oauth_client()
        create_custom_provider(provider["api_endpoint"])
        create_domain()
        set_non_email_user()
        generate_bearer_token()
        check_bearer_token(
            get_data(api_response, provider["prefix"])
            + provider["separator"]
            + get_data(api_response, provider["suffix"])
            + "@"
            + provider["domain"]
        )

        remove_bearer_tokens(
            get_data(api_response, provider["prefix"])
            + provider["separator"]
            + get_data(api_response, provider["suffix"])
            + "@"
            + provider["domain"]
        )
        remove_custom_provider()
        remove_oauth_client()
        remove_user(
            get_data(api_response, provider["prefix"])
            + provider["separator"]
            + get_data(api_response, provider["suffix"])
            + "@"
            + provider["domain"]
        )
        remove_domain()

    def test_email_user_create_session(self):
        fetch_endpoint_data(provider["email_api_endpoint_url"])
        create_oauth_client()
        create_custom_provider(provider["email_api_endpoint_url"], True)
        response = generate_bearer_token()
        check_bearer_token(get_data(api_response, provider["email_map"]))
        verify_session_user(response)

        remove_bearer_tokens(get_data(api_response, provider["email_map"]))
        remove_custom_provider()
        remove_oauth_client()
        remove_user(get_data(api_response, provider["email_map"]))
