import unittest

import frappe
from frappe.exceptions import ValidationError

# List to store the records and doctypes to be deleted
# Sequence is IMPORTANT
tear_down_data = {
    "User Permission": [],
    "User Permission Rule": [],
    "Permission Rule": [],
    "Custom Invoice": [],
    "Custom Customer": [],
    "Custom Warehouse": [],
    "Custom Location": [],
    "User": [],
    "DocType": [],
}

number_of_doc = 3


def set_up_doctype():
    # Customer doctype
    customer_doctype = frappe.get_doc(
        {
            "doctype": "DocType",
            "module": "Frappe Utils",
            "naming_rule": "Expression",
            "autoname": "format:{name1}",
            "fields": [
                {
                    "fieldname": "name1",
                    "label": "Name",
                    "fieldtype": "Data",
                    "reqd": 1,
                },
            ],
            "permissions": [
                {
                    "role": "Sales Manager",
                    "read": 1,
                    "write": 0,
                    "create": 0,
                    "delete": 0,
                },
                {
                    "role": "System Manager",
                    "read": 1,
                    "write": 1,
                    "create": 1,
                    "delete": 1,
                },
            ],
        }
    )
    customer_doctype.name = "Custom Customer"
    customer_doctype.insert()
    tear_down_data["DocType"].append(customer_doctype.name)

    # Warehouse doctype
    warehouse_doctype = frappe.get_doc(
        {
            "doctype": "DocType",
            "module": "Frappe Utils",
            "naming_rule": "Expression",
            "autoname": "format:{name1}",
            "fields": [
                {
                    "fieldname": "name1",
                    "label": "Name",
                    "fieldtype": "Data",
                    "reqd": 1,
                },
            ],
            "permissions": [
                {
                    "role": "Sales Manager",
                    "read": 1,
                    "write": 0,
                    "create": 0,
                    "delete": 0,
                },
                {
                    "role": "System Manager",
                    "read": 1,
                    "write": 1,
                    "create": 1,
                    "delete": 1,
                },
            ],
        }
    )
    warehouse_doctype.name = "Custom Warehouse"
    warehouse_doctype.insert()
    tear_down_data["DocType"].append(warehouse_doctype.name)

    # Location doctype
    location_doctype = frappe.get_doc(
        {
            "doctype": "DocType",
            "module": "Frappe Utils",
            "naming_rule": "Expression",
            "autoname": "format:{name1}",
            "fields": [
                {
                    "fieldname": "name1",
                    "label": "Name",
                    "fieldtype": "Data",
                    "reqd": 1,
                },
            ],
            "permissions": [
                {
                    "role": "Sales Manager",
                    "read": 1,
                    "write": 0,
                    "create": 0,
                    "delete": 0,
                },
                {
                    "role": "System Manager",
                    "read": 1,
                    "write": 1,
                    "create": 1,
                    "delete": 1,
                },
            ],
        }
    )
    location_doctype.name = "Custom Location"
    location_doctype.insert()
    tear_down_data["DocType"].append(location_doctype.name)

    # Invoice doctype
    invoice_doctype = frappe.get_doc(
        {
            "doctype": "DocType",
            "module": "Frappe Utils",
            "naming_rule": "Expression",
            "autoname": "format:{name1}",
            "fields": [
                {
                    "fieldname": "name1",
                    "label": "Name",
                    "fieldtype": "Data",
                    "reqd": 1,
                },
                {
                    "fieldname": "customer",
                    "label": "Customer",
                    "fieldtype": "Link",
                    "options": "Custom Customer",
                },
                {
                    "fieldname": "warehouse",
                    "label": "Warehouse",
                    "fieldtype": "Link",
                    "options": "Custom Warehouse",
                },
                {
                    "fieldname": "location",
                    "label": "Location",
                    "fieldtype": "Link",
                    "options": "Custom Location",
                },
            ],
            "permissions": [
                {
                    "role": "Sales Manager",
                    "read": 1,
                    "write": 0,
                    "create": 0,
                    "delete": 0,
                },
                {
                    "role": "System Manager",
                    "read": 1,
                    "write": 1,
                    "create": 1,
                    "delete": 1,
                },
            ],
        }
    )
    invoice_doctype.name = "Custom Invoice"
    invoice_doctype.insert()
    tear_down_data["DocType"].append(invoice_doctype.name)


def create_entries_in_custom_doctypes(custom_doctype):
    for i in range(number_of_doc):
        new_doc = frappe.get_doc(
            {
                "doctype": f"Custom {custom_doctype}",
                "name1": f"{custom_doctype} {i+1}",
            }
        )
        new_doc.insert()
        tear_down_data[("Custom " + custom_doctype)].append(new_doc.name)


def create_entries_in_custom_invoice():
    invoice_doc = frappe.get_doc(
        {
            "doctype": "Custom Invoice",
            "name1": "invoice 1",
            "customer": "customer 1",
            "warehouse": "warehouse 1",
            "location": "location 1",
        }
    )
    invoice_doc.insert()
    tear_down_data["Custom Invoice"].append(invoice_doc.name)

    invoice_doc = frappe.get_doc(
        {
            "doctype": "Custom Invoice",
            "name1": "invoice 2",
            "customer": "customer 1",
            "warehouse": "warehouse 1",
            "location": "location 2",
        }
    )
    invoice_doc.insert()
    tear_down_data["Custom Invoice"].append(invoice_doc.name)

    invoice_doc = frappe.get_doc(
        {
            "doctype": "Custom Invoice",
            "name1": "invoice 3",
            "customer": "customer 1",
            "warehouse": "warehouse 1",
            "location": "location 3",
        }
    )
    invoice_doc.insert()
    tear_down_data["Custom Invoice"].append(invoice_doc.name)


def create_perm_rule(name, for_doctype, perm_rule_links, granular_perm):
    payload = {
        "permission_rule_name": name,
        "for_doctype": for_doctype,
        "doctype": "Permission Rule",
        "granular_permission_check": 1,
        "granular_permission_select": 1,
        "granular_permission_read": 1,
        "granular_permission_write": granular_perm[0],
        "granular_permission_create": granular_perm[1],
        "granular_permission_delete": granular_perm[2],
        "granular_permission_submit": granular_perm[3],
        "granular_permission_cancel": granular_perm[4],
        "granular_permission_amend": granular_perm[5],
        "permission_rule_links": [
            {
                "link_doctype": "Custom Customer",
                "link_name": perm_rule_links[0],
                "parentfield": "permission_rule_links",
                "parenttype": "Permission Rule",
                "doctype": "Permission Rule Link",
            },
            {
                "link_doctype": "Custom Warehouse",
                "link_name": perm_rule_links[1],
                "parentfield": "permission_rule_links",
                "parenttype": "Permission Rule",
                "doctype": "Permission Rule Link",
            },
            {
                "link_doctype": "Custom Location",
                "link_name": perm_rule_links[2],
                "parentfield": "permission_rule_links",
                "parenttype": "Permission Rule",
                "doctype": "Permission Rule Link",
            },
        ],
    }
    new_doc = frappe.get_doc(payload)
    new_doc.insert()
    frappe.db.commit()
    tear_down_data["Permission Rule"].append(new_doc.name)


def assign_perm_rule(user, for_doctype, permission_rule_name):
    new_doc = frappe.get_doc(
        {
            "user": user,
            "for_doctype": for_doctype,
            "doctype": "User Permission Rule",
            "permission_rules": [
                {
                    "permission_rule": permission_rule_name,
                }
            ],
        }
    )
    new_doc.insert()
    frappe.db.commit()
    tear_down_data["User Permission Rule"].append(new_doc.name)


def create_user(name, email):
    user_doc = frappe.get_doc(
        {
            "doctype": "User",
            "first_name": name,
            "email": email,
            "roles": [{"role": "System Manager"}],
        }
    )
    user_doc.insert()
    if user_doc.first_name != "Anonymous":
        # Do not delete the Dummy User
        tear_down_data["User"].append(user_doc.email)


class TestCompositeKey(unittest.TestCase):
    frappe.set_user("Administrator")
    exists = frappe.db.exists("User", "anonymous@gmail.com")
    if not exists:
        # Create a Dummy User with System Manager rights
        # if not present already and forget it
        create_user("Anonymous", "anonymous@gmail.com")

    def setUp(self):
        set_up_doctype()
        create_entries_in_custom_doctypes("Customer")
        create_entries_in_custom_doctypes("Warehouse")
        create_entries_in_custom_doctypes("Location")
        create_entries_in_custom_invoice()
        create_user("New User", "newuser@gmail.com")

    def tearDown(self):
        # DB Cleaning goes here....
        frappe.set_user("Administrator")

        # Remove all other doctypes
        for key, array in tear_down_data.items():
            for element in array:
                frappe.delete_doc(key, element)

    def test_perm_rule_read(self):
        # Create perm rules and assign to user
        create_perm_rule(
            "invoice rule 01",
            "Custom Invoice",
            ["customer 1", "warehouse 1", "location 1"],
            [0, 0, 0, 0, 0, 0],
        )
        assign_perm_rule(
            "newuser@gmail.com", "Custom Invoice", "invoice rule 01"  # noqa: E501
        )

        create_perm_rule(
            "invoice rule 02",
            "Custom Invoice",
            ["customer 1", "warehouse 1", "location 2"],
            [0, 0, 0, 0, 0, 0],
        )
        assign_perm_rule(
            "newuser@gmail.com", "Custom Invoice", "invoice rule 02"  # noqa: E501
        )

        frappe.set_user(tear_down_data.get("User")[0])
        # Get the list of invoice that user can see
        invoice_from_get_list = frappe.get_list("Custom Invoice")

        expected_invoice = [{"name": "invoice 1"}, {"name": "invoice 2"}]

        self.assertCountEqual(
            expected_invoice,
            invoice_from_get_list,
            f"\n expected invoice should be {expected_invoice} but user can see {invoice_from_get_list}",  # noqa: E501
        )

    def test_perm_rule_write(self):
        # Create perm rules and assign to user
        create_perm_rule(
            "invoice rule 01",
            "Custom Invoice",
            ["customer 1", "warehouse 1", "location 1"],
            [0, 0, 0, 0, 0, 0],  # can read
        )
        assign_perm_rule(
            "newuser@gmail.com", "Custom Invoice", "invoice rule 01"  # noqa: E501
        )

        create_perm_rule(
            "invoice rule 02",
            "Custom Invoice",
            ["customer 1", "warehouse 1", "location 2"],
            [1, 0, 0, 0, 0, 0],  # can read & write
        )
        assign_perm_rule(
            "newuser@gmail.com", "Custom Invoice", "invoice rule 02"  # noqa: E501
        )

        frappe.set_user(tear_down_data.get("User")[0])

        invoice1_doc = frappe.get_doc("Custom Invoice", "invoice 1")
        invoice2_doc = frappe.get_doc("Custom Invoice", "invoice 2")

        try:
            invoice2_doc.name1 = "invoice 002"
            invoice2_doc.save()
            frappe.db.commit()
            self.assertTrue(True, "")

        except ValidationError:
            self.assertFalse(
                True,
                "Unable to edit record when 'write' permission is granted.",
            )

        else:
            try:
                invoice1_doc.name1 = "invoice 001"
                invoice1_doc.save()
                frappe.db.commit()
                self.assertFalse(
                    True,
                    "It is able to edit record when 'write' permission is not granted.",  # noqa: E501
                )

            except Exception:
                self.assertTrue(True, "")

    def test_perm_rule_delete(self):
        # Create perm rules and assign to user
        create_perm_rule(
            "invoice rule 01",
            "Custom Invoice",
            ["customer 1", "warehouse 1", "location 1"],
            [0, 0, 0, 0, 0, 0],  # can read
        )
        assign_perm_rule(
            "newuser@gmail.com", "Custom Invoice", "invoice rule 01"  # noqa: E501
        )

        create_perm_rule(
            "invoice rule 02",
            "Custom Invoice",
            ["customer 1", "warehouse 1", "location 2"],
            [0, 0, 1, 0, 0, 0],  # can read & delete
        )
        assign_perm_rule(
            "newuser@gmail.com", "Custom Invoice", "invoice rule 02"  # noqa: E501
        )

        frappe.set_user(tear_down_data.get("User")[0])

        try:
            frappe.delete_doc("Custom Invoice", "invoice 2")
            frappe.db.commit()
            self.assertTrue(True, "")

        except ValidationError:
            self.assertFalse(
                True,
                "unable to delete record when 'delete' permission is granted.",  # noqa: E501
            )

        else:
            try:
                frappe.delete_doc("Custom Invoice", "invoice 1")
                frappe.db.commit()
                self.assertFalse(
                    True,
                    "It is able to delete record when 'delete' permission is not granted.",  # noqa: E501
                )

            except Exception:
                self.assertTrue(True, "")
