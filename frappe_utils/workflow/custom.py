import frappe
from frappe import enqueue
from frappe.model.workflow import get_workflow_name

from frappe.workflow.doctype.workflow_action.workflow_action import (  # noqa: E501 # isort:skip
    clear_doctype_notifications,
    clear_workflow_actions,
    create_workflow_actions_for_roles,
    deduplicate_actions,
    get_common_email_args,
    get_doc_workflow_state,
    get_next_possible_transitions,
    get_users_next_action_data,
    is_workflow_action_already_created,
    update_completed_workflow_actions,
)


def send_workflow_action_email(users_data, doc):
    common_args = get_common_email_args(doc)
    message = common_args.pop("message", None)
    # TODO: filter users_data as per user permissions
    for d in users_data:
        if frappe.has_permission(doc=doc, user=d.get("email")):
            email_args = {
                "recipients": [d.get("email")],
                "args": {
                    "actions": list(
                        deduplicate_actions(d.get("possible_actions"))
                    ),  # noqa: E501
                    "message": message,
                },
                "reference_name": doc.name,
                "reference_doctype": doc.doctype,
            }
            email_args.update(common_args)
            try:
                frappe.sendmail(**email_args)
            except frappe.OutgoingEmailError:
                # Emails config broken, don't bother retrying next user.
                frappe.log_error("Failed to send workflow action email")
                return


def process_workflow_actions(doc, state):
    workflow = get_workflow_name(doc.get("doctype"))
    if not workflow:
        return

    if state == "on_trash":
        clear_workflow_actions(doc.get("doctype"), doc.get("name"))
        return

    if is_workflow_action_already_created(doc):
        return

    update_completed_workflow_actions(
        doc, workflow=workflow, workflow_state=get_doc_workflow_state(doc)
    )
    clear_doctype_notifications("Workflow Action")

    next_possible_transitions = get_next_possible_transitions(
        workflow, get_doc_workflow_state(doc), doc
    )

    if not next_possible_transitions:
        return

    user_data_map, roles = get_users_next_action_data(
        next_possible_transitions, doc
    )  # noqa: E501

    if not user_data_map:
        return

    create_workflow_actions_for_roles(roles, doc)

    if frappe.get_cached_value(
        "Workflow", workflow, "custom_send_granular_email_alert"
    ):  # noqa: E501
        enqueue(
            send_workflow_action_email,
            queue="short",
            users_data=list(user_data_map.values()),
            doc=doc,
        )
